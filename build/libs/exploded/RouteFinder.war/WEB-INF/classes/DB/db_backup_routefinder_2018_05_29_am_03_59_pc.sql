-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: routefinder
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `id` int(11) DEFAULT NULL,
  `address` varchar(1010) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (0,'A.D. Gordon Street 12, Jerusalem'),(1,'He-Khaluts Street 12, Jerusalem'),(2,'A.D. Gordon Street 18, Jerusalem'),(3,'Borochov Street 18, Jerusalem'),(4,'Borochov Street 26, Jerusalem'),(5,'Borochov Street 32, Jerusalem'),(6,'Borochov Street 38, Jerusalem'),(7,'Borochov Street 50, Jerusalem'),(8,'Gelber Street 1, Jerusalem'),(9,'Gelber Street 5, Jerusalem'),(10,'Gelber Street 8, Jerusalem'),(11,'Gelber Street 12, Jerusalem'),(12,'Gelber Street 15, Jerusalem'),(13,'Gelber Street 20, Jerusalem'),(14,'HaAyal Street 1, Jerusalem'),(15,'HaAyal Street 4, Jerusalem'),(16,'HaAyal Street 7, Jerusalem'),(17,'HaAyal Street 11, Jerusalem'),(18,'HaKfir Street 1, Jerusalem'),(19,'HaKfir Street 3, Jerusalem'),(20,'HaKfir Street 7, Jerusalem'),(21,'HaKfir Street 9, Jerusalem'),(22,'HaKfir Street 14, Jerusalem'),(23,'Horkanya Street 20, Jerusalem'),(24,'Horkanya Street 3, Jerusalem'),(25,'Horkanya Street 10, Jerusalem'),(26,'Horkanya Street 12, Jerusalem'),(27,'Horkanya Street 17, Jerusalem');
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `current`
--

DROP TABLE IF EXISTS `current`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `current` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `per_time` varchar(110) NOT NULL,
  `starting_address` varchar(1010) NOT NULL,
  `total_time` varchar(110) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `current_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `current`
--

LOCK TABLES `current` WRITE;
/*!40000 ALTER TABLE `current` DISABLE KEYS */;
INSERT INTO `current` VALUES (1,'5','A.D. Gordon Street 12, Jerusalem','200');
/*!40000 ALTER TABLE `current` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `boy_id` int(11) DEFAULT NULL,
  `loc_str` varchar(10000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (0,'0_-1_0.0_0:2_0_95.4826886720359_186:4_2_201.80115000226317_126:7_4_288.1814567813037_302:12_7_815.7486360418003_508:10_12_897.3370411235849_482:8_10_942.4238856236822_469:16_8_1676.1245331526816_625:22_16_1741.4974259497112_626:19_22_1746.056843593729_621:21_19_1758.0746344232919_624:1_21_2225.695275321726_477:23_1_2616.935244734327_769:26_23_2651.7929149772863_793:24_26_2724.005472691735_743'),(1,'0_-1_0.0_0:6_0_152.66091102913649_157:5_6_174.3407598574581_143:3_5_221.264388266219_102:13_3_795.9647219369169_512:11_13_858.2767453978594_489:9_11_913.7083939754729_479:17_9_1665.3050449182983_629:15_17_1684.0613060130959_623:18_15_1743.3048273719007_620:20_18_1756.3381276670807_623:14_20_1774.6568184685684_729:27_14_2619.274218077863_788:25_27_2675.2453239801066_765');
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(110) NOT NULL,
  `password` varchar(110) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  `designation` varchar(110) DEFAULT NULL,
  `details` varchar(110) DEFAULT NULL,
  `user_type` varchar(110) NOT NULL DEFAULT 'employee',
  `assigned_manager` int(11) DEFAULT '0',
  `branch` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_uindex` (`id`),
  UNIQUE KEY `user_username_uindex` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-29  3:59:58
