<%@ page import="com.routefinder.connection.Database" %>
<%@ page import="com.routefinder.web.util.Utils" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashSet" %>
<%@ page import="java.util.Set" %><%--
  Created by IntelliJ IDEA.
  User: md_al
  Date: 14-Apr-18
  Time: 9:47 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <!------ Include the above in your HEAD tag ---------->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <%--<script>
        $('button').click(function(){
            $('#pTest').text('test')
        })
    </script>--%>

    <script>
        function showOrHideManager() {
            if(document.getElementsByName("user_type")[0].value === "employee") {
                document.getElementById("manager").style.display = "block";
            } else {
                document.getElementById("manager").style.display = "none";
            }
        }
    </script>
    <style>
        .btn-file {
            position: relative;
            overflow: hidden;
        }
        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }
    </style>

    <style>

        body{
            background-image:url("resources/assets/img/backgrounds/1.jpg");/*url(https://static.pexels.com/photos/371633/pexels-photo-371633.jpeg);*/
            background-repeat:no-repeat;
            background-size: 100%;
            font: 400 15px/1.8 Lato, sans-serif;
            color: #69bb5a;
        }



        h3, h4 {
            margin: 10px 0 30px 0;
            letter-spacing: 10px;
            font-size: 20px;
            color: #111;
        }
        .container {
            padding: 80px 120px;
        }
        .person {
            border: 10px solid transparent;
            margin-bottom: 25px;
            width: 80%;
            height: 80%;
            opacity: 0.7;
        }
        .person:hover {
            border-color: #f1f1f1;
        }
        .carousel-inner img {
            -webkit-filter: grayscale(90%);
            filter: grayscale(90%); /* make all photos black and white */
            width: 100%; /* Set width to 100% */
            margin: auto;
        }
        .carousel-caption h3 {
            color: #fff !important;
        }
        @media (max-width: 600px) {
            .carousel-caption {
                display: none; /* Hide the carousel text when the screen is less than 600 pixels wide */
            }
        }
        .bg-1 {
            background: #2d2d30;
            color: #bdbdbd;
        }
        .bg-1 h3 {color: #fff;}
        .bg-1 p {font-style: italic;}
        .list-group-item:first-child {
            border-top-right-radius: 0;
            border-top-left-radius: 0;
        }
        .list-group-item:last-child {
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }
        .thumbnail {
            padding: 0 0 15px 0;
            border: none;
            border-radius: 0;
        }
        .thumbnail p {
            margin-top: 15px;
            color: #555;
        }
        .btn {
            padding: 10px 20px;
            background-color: #333;
            color: #f1f1f1;
            border-radius: 0;
            transition: .2s;
        }
        .btn:hover, .btn:focus {
            border: 1px solid #333;
            background-color: #fff;
            color: #000;
        }
        .modal-header, h4, .close {
            background-color: #333;
            color: #fff !important;
            text-align: center;
            font-size: 30px;
        }
        .modal-header, .modal-body {
            padding: 40px 50px;
        }
        .nav-tabs li a {
            color: #777;
        }
        #googleMap {
            width: 100%;
            height: 400px;
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%);
        }
        .navbar {
            font-family: Montserrat, sans-serif;
            margin-bottom: 0;
            background-color: #2d2d30;
            border: 0;
            font-size: 11px !important;
            letter-spacing: 4px;
            opacity: 0.9;
        }
        .navbar li a, .navbar .navbar-brand {
            color: #d5d5d5 !important;
        }
        .navbar-nav li a:hover {
            color: #fff !important;
        }
        .navbar-nav li.active a {
            color: #fff !important;
            background-color: #29292c !important;
        }
        .navbar-default .navbar-toggle {
            border-color: transparent;
        }
        .open .dropdown-toggle {
            color: #fff;
            background-color: #555 !important;
        }
        .dropdown-menu li a {
            color: #000 !important;
        }
        .dropdown-menu li a:hover {
            background-color: red !important;
        }
        footer {
            background-color: #2d2d30;
            color: #f5f5f5;
            padding: 32px;
        }
        footer a {
            color: #f5f5f5;
        }
        footer a:hover {
            color: #777;
            text-decoration: none;
        }
        .form-control {
            border-radius: 0;
        }
        textarea {
            resize: none;
        }

        #map {
            height: 800px;
            width: 100%;
        }
    </style>
    <link rel="stylesheet" href="resources/table-responsive/css/style.css">

    <script>
        $(document).ready(function() {
            $.ajaxSetup({ cache: false }); // This part addresses an IE bug.  without it, IE will only load the first number and will never refresh
            setInterval(function() {
                /*$('#maindiv').load('tracking');*/
                <%if(!Utils.isEmplyOrNull(request.getParameter("userId"))&&!Utils.isEmplyOrNull(request.getParameter("date"))&&!Utils.isEmplyOrNull(request.getParameter("liveTracking"))){%>
                history.go(0);
                <%}%>
            }, 20000); // the "3000"
        });




        var rad = function(x) {
            return x * Math.PI / 180;
        };

        var getDistance = function(lat1, lng1, lat2, lng2) {
            var R = 6378137; // Earth’s mean radius in meter
            var dLat = rad(lat2 - lat1);
            var dLong = rad(lng2 - lng1);
            var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(rad(lat1)) * Math.cos(rad(lat2)) *
                Math.sin(dLong / 2) * Math.sin(dLong / 2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            var d = R * c;
            return d; // returns the distance in meter
        };
    </script>

    <title>Dashboard</title>
</head>



<body>
<%
    if(Utils.isEmplyOrNull(session.getAttribute("username"))) {
        response.sendRedirect("login");
    }

    Database db = new Database();
    db.connect();

    try{

        boolean isAdmin = !Utils.isEmplyOrNull(session.getAttribute("userType")) && session.getAttribute("userType").equals("admin");



%>



<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header navbar-center">

            <a class="navbar-brand" href="dashboard">Dashboard <span class="sr-only">(current)</span></a>
            <a class="navbar-brand" href="history">History <span class="sr-only">(current)</span></a>
            <a class="navbar-brand" href="tracking">Tracking <span class="sr-only">(current)</span></a>
            <a class="navbar-brand" href="profile?id=<%=session.getAttribute("id")%>">Profile <span class="sr-only">(current)</span></a>
            <%if(isAdmin){%><a class="navbar-brand" href="profile">Insert <span class="sr-only">(current)</span></a><%}%>

        </div>
        <div class="navbar-header navbar-right">
            <a class="navbar-brand" href="login">Logout (<%=session.getAttribute("username")%>)</a>
        </div>
    </div>
</nav>


<%


        String assignedManagerQry = "";
        if(!Utils.isEmplyOrNull(request.getParameter("assignedManager"))) {
            assignedManagerQry+=" and assigned_manager="+request.getParameter("assignedManager");
        }

        Statement st2 = db.connection.createStatement();
        String q2 = "select * from user where id>0 and user_type='employee'";
        if(!isAdmin) q2+=" and assigned_manager="+session.getAttribute("id");
        q2+=assignedManagerQry;
        ResultSet rs2 = st2.executeQuery(q2);
        Set<String> alloweUserIdList = new HashSet<>();
        String allowedUserIdListStr = "";
        while (rs2.next()) {
            alloweUserIdList.add(rs2.getString("id"));
            allowedUserIdListStr+=","+rs2.getString("id");
        }
        rs2.beforeFirst();

        String q = "select * from event where id>0 ";


        if(!Utils.isEmplyOrNull(request.getParameter("userId"))) {
            if(alloweUserIdList.contains(request.getParameter("userId"))) {
                q += " and user_id=" + request.getParameter("userId");
            } else {
                q += " and user_id=-1";
            }
        } else {
            q+=" and user_id in (-1"+allowedUserIdListStr+")";
        }
        if(!Utils.isEmplyOrNull(request.getParameter("date"))) {
            q+=" and event_time like '"+request.getParameter("date").substring(0,10)+"%'";
        }

        q+=" ORDER by event_time desc";


    Statement st3 = db.connection.createStatement();
    String q3 = "select * from user where id>0 and user_type='manager'";
    ResultSet rs3= st3.executeQuery(q3);


%>
<main>
    <div id="maindiv" class="container my-5">
        <%

            Statement st = db.connection.createStatement();
            ResultSet rs = st.executeQuery(q);
            String centerLat = "23.363";
            String centerLong = "90.044";
            double distCovered = 0.0;
            if(rs.next()) {
                centerLat = rs.getString("latitude");
                centerLong = rs.getString("longitude");
                distCovered = 0.0;
            }
        %>

        <div class="card-body text-center alert-default">
            <h2 class="card-title">Employee Tracker</h2>
            <h3 class="card-text">Navigation Tracking</h3>
        </div>
        <div class="card">

            <%if(isAdmin){%>
            <form method="get" action="tracking">
                <div class="row">

                    <div class="col col-sm-2">Tracking Employee(s): </div>
                    <div class="col col-sm-3">
                        <select name="assignedManager" class="form-control">
                            <option value="">--Select Manager--</option>
                            <%while (rs3.next()) {%>
                            <option value="<%=rs3.getString("id")%>"><%=rs3.getString("name")%></option>
                            <%}%>
                        </select>
                    </div>
                    <div class="col col-sm-2"> <button type="submit" class="form-control btn btn-primary">Set</button></div>

                </div>
            </form>
            <%}%>


            <form method="get" action="tracking">
            <div class="row">

                    <div class="col col-sm-2">Tracking Employee(s): </div>
                <div class="col col-sm-3">
                    <select name="userId" class="form-control">
                        <option value="">Select Employee</option>
                        <%while (rs2.next()) {%>
                            <option value="<%=rs2.getString("id")%>"><%=rs2.getString("name")%></option>
                        <%}rs2.beforeFirst();%>
                    </select>
                </div>
                    <div class="col col-sm-2">Tracking Date: </div>
                <div class="col col-sm-3">
                        <input type="date" name="date" class="form-control"/>
                </div>
                    <div class="col col-sm-2"> <button type="submit" class="form-control btn btn-primary">Search</button></div>

            </div>
            </form>

            <form method="get" action="tracking">
                <div class="row">

                    <div class="col col-sm-2">Tracking Employee(s): </div>
                    <div class="col col-sm-3">
                        <select name="userId" class="form-control" required>
                            <option value="">Select Employee</option>
                            <%while (rs2.next()) {%>
                            <option value="<%=rs2.getString("id")%>"><%=rs2.getString("name")%></option>
                            <%}%>
                        </select>
                    </div>
                    <div class="col col-sm-2">Tracking Date: </div>
                    <div class="col col-sm-3">
                        <input type="date" name="date" class="form-control" required/>
                        <input type="hidden" name="liveTracking" value="true">
                    </div>
                    <div class="col col-sm-2"> <button type="submit" class="form-control btn btn-primary">Tracking</button></div>

                </div>
            </form>

            <div id="map">
            </div>
            <script>

                function initMap() {

                    var infowindow = new google.maps.InfoWindow();
                    var i=0;
                    var center = new google.maps.LatLng(<%=centerLat%>, <%=centerLong%>);
                    var map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 16,
                        center: center
                    });
                    var distCovered = 0.0;
                    <%
                    rs.beforeFirst();
                    while (rs.next()) {%>
                    (function(){

                        var icon = {
                            url: "resources/images/<%=rs.getString("user_id")%>.jpg", // url
                            scaledSize: new google.maps.Size(40, 40), // scaled size
                            origin: new google.maps.Point(0, 0), // origin
                            anchor: new google.maps.Point(0, 0) // anchor
                        };

                        var markerIconGlastonbury = {
                            url: "resources/images/<%=rs.getString("user_id")%>.jpg",
                            //The size image file.
                            size: new google.maps.Size(225, 120),
                            //The point on the image to measure the anchor from. 0, 0 is the top left.
                            origin: new google.maps.Point(0, 0),
                            //The x y coordinates of the anchor point on the marker. e.g. If your map marker was a drawing pin then the anchor would be the tip of the pin.
                            anchor: new google.maps.Point(<%=rs.getString("latitude")%>, <%=rs.getString("longitude")%>)
                        };

                        //Setting the shape to be used with the Glastonbury map marker.
                        var markerShapeGlastonbury = {
                            coord: [12,4,216,22,212,74,157,70,184,111,125,67,6,56],
                            type: 'poly'
                        };

                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(<%=rs.getString("latitude")%>, <%=rs.getString("longitude")%>),
                        map: map,
                        icon: icon,
                        shape: markerShapeGlastonbury,
                        //sets the z-index of the map marker.
                        zIndex:102
                    });


                    <%if(!Utils.isEmplyOrNull(request.getParameter("liveTracking"))){%>

                        var flightPath = new google.maps.Polyline({
                            path: [{lat:<%=centerLat%>, lng:<%=centerLong%>},
                            {lat:<%=rs.getString("latitude")%>, lng:<%=rs.getString("longitude")%>}],
                            geodesic: true,
                            strokeColor: '#FF0000',
                            strokeOpacity: 1.0,
                            strokeWeight: 2
                        });
                        flightPath.setMap(map);
                        distCovered+=getDistance(<%=centerLat%>, <%=centerLong%>,<%=rs.getString("latitude")%>, <%=rs.getString("longitude")%>);
                        <%

                        centerLat = rs.getString("latitude");
                        centerLong = rs.getString("longitude");
                        %>
                        <%}%>

                        marker.addListener('click', function() {

                            map.setCenter(marker.getPosition());
                        });

                        google.maps.event.addListener(marker, 'click', (function (marker, i) {
                            return function () {

                                infowindow.setContent("<strong>time : "+"<%=rs.getString("event_time")%>"+"</strong>"+"<br>"+"<b>event : "+"<%=rs.getString("event_type")%>"+"</b><br><b>Distance Covered: "+distCovered+" meters</b>");

                                infowindow.open(map, marker);

                            }
                        })(marker, i));
i++;
                    })();
                    <%}%>
                }
            </script>
            <script async defer
                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCtd1rYhZdAjHp30HEBYhxTWa_TFRcC5vg&callback=initMap">
            </script>


        </div>
        <!-- Large modal -->


        <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="card-body text-center">
                        <h4 class="card-title">Special title treatment</h4>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                    </div>
                    <div class=" card col-8 offset-2 my-2 p-3">
                        <form>
                            <div class="form-group">
                                <label for="listname">List name</label>
                                <input type="text" class="form-control" name="listname" id="listname" placeholder="Enter your listname">
                            </div>
                            <div class="form-group">
                                <label for="datepicker">Deadline</label>
                                <input  type="text" class="form-control" name="datepicker" id="datepicker" placeholder="Pick up a date">
                            </div>
                            <div class="form-group">
                                <label for="datepicker">Add a list item</label>
                                <div class="input-group">

                                    <input type="text" class="form-control" placeholder="Add an item" aria-label="Search for...">
                                    <span class="input-group-btn">
                    <button class="btn btn-secondary" type="button">Go!</button>
                  </span>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-block btn-primary">Sign in</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!---->

<!-- Footer -->
<footer class="text-center">
    <a class="up-arrow" href="#myPage" data-toggle="tooltip" title="TO TOP">
        <span class="glyphicon glyphicon-chevron-up"></span>
    </a><br><br>
    <p>Made By <a href="https://www.w3schools.com" data-toggle="tooltip" title="Visit w3schools">Employee Tracker</a></p>
</footer>
<%} catch (Exception e) {
    System.out.println(e);
    } finally {
    db.close();
    }

%>
</body>
</html>
