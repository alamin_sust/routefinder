<%@ page import="com.routefinder.web.util.Utils" %>
<%@ page import="com.routefinder.connection.Database" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="javax.management.Query" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.TreeMap" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %><%--
  Created by IntelliJ IDEA.
  User: md_al
  Date: 14-Apr-18
  Time: 4:23 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <!------ Include the above in your HEAD tag ---------->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <%--<script>
        $('button').click(function(){
            $('#pTest').text('test')
        })
    </script>--%>

    <script>
        function showOrHideManager() {
            if(document.getElementsByName("user_type")[0].value === "employee") {
                document.getElementById("manager").style.display = "block";
            } else {
                document.getElementById("manager").style.display = "none";
            }
        }
    </script>
    <style>
        .btn-file {
            position: relative;
            overflow: hidden;
        }
        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }
    </style>

    <style>

        body{
            background-image:url("resources/assets/img/backgrounds/1.jpg");/*url(https://static.pexels.com/photos/371633/pexels-photo-371633.jpeg);*/
            background-repeat:no-repeat;
            background-size: 100%;
            font: 400 15px/1.8 Lato, sans-serif;
            color: #69bb5a;
        }



        h3, h4 {
            margin: 10px 0 30px 0;
            letter-spacing: 10px;
            font-size: 20px;
            color: #111;
        }
        .container {
            padding: 80px 120px;
        }
        .person {
            border: 10px solid transparent;
            margin-bottom: 25px;
            width: 80%;
            height: 80%;
            opacity: 0.7;
        }
        .person:hover {
            border-color: #f1f1f1;
        }
        .carousel-inner img {
            -webkit-filter: grayscale(90%);
            filter: grayscale(90%); /* make all photos black and white */
            width: 100%; /* Set width to 100% */
            margin: auto;
        }
        .carousel-caption h3 {
            color: #fff !important;
        }
        @media (max-width: 600px) {
            .carousel-caption {
                display: none; /* Hide the carousel text when the screen is less than 600 pixels wide */
            }
        }
        .bg-1 {
            background: #2d2d30;
            color: #bdbdbd;
        }
        .bg-1 h3 {color: #fff;}
        .bg-1 p {font-style: italic;}
        .list-group-item:first-child {
            border-top-right-radius: 0;
            border-top-left-radius: 0;
        }
        .list-group-item:last-child {
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }
        .thumbnail {
            padding: 0 0 15px 0;
            border: none;
            border-radius: 0;
        }
        .thumbnail p {
            margin-top: 15px;
            color: #555;
        }
        .btn {
            padding: 10px 20px;
            background-color: #333;
            color: #f1f1f1;
            border-radius: 0;
            transition: .2s;
        }
        .btn:hover, .btn:focus {
            border: 1px solid #333;
            background-color: #fff;
            color: #000;
        }
        .modal-header, h4, .close {
            background-color: #333;
            color: #fff !important;
            text-align: center;
            font-size: 30px;
        }
        .modal-header, .modal-body {
            padding: 40px 50px;
        }
        .nav-tabs li a {
            color: #777;
        }
        #googleMap {
            width: 100%;
            height: 400px;
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%);
        }
        .navbar {
            font-family: Montserrat, sans-serif;
            margin-bottom: 0;
            background-color: #2d2d30;
            border: 0;
            font-size: 11px !important;
            letter-spacing: 4px;
            opacity: 0.9;
        }
        .navbar li a, .navbar .navbar-brand {
            color: #d5d5d5 !important;
        }
        .navbar-nav li a:hover {
            color: #fff !important;
        }
        .navbar-nav li.active a {
            color: #fff !important;
            background-color: #29292c !important;
        }
        .navbar-default .navbar-toggle {
            border-color: transparent;
        }
        .open .dropdown-toggle {
            color: #fff;
            background-color: #555 !important;
        }
        .dropdown-menu li a {
            color: #000 !important;
        }
        .dropdown-menu li a:hover {
            background-color: red !important;
        }
        footer {
            background-color: #2d2d30;
            color: #f5f5f5;
            padding: 32px;
        }
        footer a {
            color: #f5f5f5;
        }
        footer a:hover {
            color: #777;
            text-decoration: none;
        }
        .form-control {
            border-radius: 0;
        }
        textarea {
            resize: none;
        }
    </style>
    <link rel="stylesheet" href="resources/table-responsive/css/style.css">
    <title>Dashboard</title>
</head>



<body>
<%
    if(Utils.isEmplyOrNull(session.getAttribute("username"))) {
        response.sendRedirect("login");
    }

    Database db = new Database();
    db.connect();

    try{

        boolean isAdmin = !Utils.isEmplyOrNull(session.getAttribute("userType")) && session.getAttribute("userType").equals("admin");




%>



<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header navbar-center">

            <a class="navbar-brand" href="dashboard">Dashboard <span class="sr-only">(current)</span></a>
            <a class="navbar-brand" href="history">History <span class="sr-only">(current)</span></a>
            <a class="navbar-brand" href="tracking">Tracking <span class="sr-only">(current)</span></a>
            <a class="navbar-brand" href="profile?id=<%=session.getAttribute("id")%>">Profile <span class="sr-only">(current)</span></a>
            <%if(isAdmin){%><a class="navbar-brand" href="profile">Insert <span class="sr-only">(current)</span></a><%}%>

        </div>
        <div class="navbar-header navbar-right">
            <a class="navbar-brand" href="login">Logout (<%=session.getAttribute("username")%>)</a>
        </div>
    </div>
</nav>

<main>
    <div class="container my-5">

        <div class="card-body text-center alert-default">
            <h2 class="card-title">Employee Tracker</h2>
            <h3 class="card-text">History</h3>
        </div>

        <%
            Statement st0 = db.connection.createStatement();
            String q0= "select * from user where user_type='employee'";
            ResultSet rs0 = st0.executeQuery(q0);

        %>

        <form method="get" action="history">
            <%--<select name="criteria" required>
                <option value="0">All Employees</option>
                <%
                    while (rs0.next()) {
                %>
                <option value="<%=rs0.getString("id")%>"><%=rs0.getString("name")%></option>
                <%}%>
            </select>--%>
                <div class="row ">
                    <div class="col col-sm-2">
                        <b>From Date: </b>
                    </div>
                    <div class="col col-sm-3"><input type="date" name="fromDate" placeholder="From Date"
                                                     class="form-control" required/>
                    </div>
                    <div class="col col-sm-2">
                        <b>To Date: </b>
                    </div>
                    <div class="col col-sm-3">
                        <input type="date" name="toDate" placeholder="To Date" class="form-control" required/>
                    </div>
                    <div class="col col-sm-2">
                        <button type="submit" class="btn btn-primary"> Search</button>
                    </div>
                </div>
        </form>

        <%


            Statement st = db.connection.createStatement();
            String q = "select * from event where event_type!='active'";

            String searchDateRange = "Search Date: ";

            if(Utils.isEmplyOrNull(request.getParameter("fromDate")) || Utils.isEmplyOrNull(request.getParameter("toDate"))) {



                SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
                Date now = new Date();
                String strDate = sdfDate.format(now).substring(0,10);
                searchDateRange +=strDate;
                q+=" and event_time like '"+strDate+"%'";
            } else {
                searchDateRange += request.getParameter("fromDate")+" to "+request.getParameter("toDate");
                q+=" and event_time >= '"+request.getParameter("fromDate")+"%' and event_time <= '"+request.getParameter("toDate")+"%'";
            }

            ResultSet rs = st.executeQuery(q);

            Map<Integer,Long> mppDuration = new TreeMap<>();

            Map<Integer,Long> mppCurrent = new TreeMap<>();

            while (rs.next()) {
                int id= rs.getInt("user_id");
                long milis = rs.getTimestamp("event_time").getTime();
                String eventType = rs.getString("event_type");

                if(eventType.equals("pause") || eventType.equals("end")) {
                    if(mppCurrent.containsKey(id)) {
                        if(!mppDuration.containsKey(id)) {
                            mppDuration.put(id,milis-mppCurrent.get(id));
                        } else {
                            mppDuration.put(id,mppDuration.get(id)+milis-mppCurrent.get(id));
                        }
                        mppCurrent.put(id,null);
                    }
                } else {
                    mppCurrent.put(id,milis);
                }
            }
        %>

        <div class="panel panel-body"><h3><%=searchDateRange%></h3></div>

        <div class="card">


            <%--<button id="add__new__list" type="button" class="btn btn-success position-absolute" data-toggle="modal" data-target=".bd-example-modal-lg"><i class="fas fa-plus"></i> Add a new List</button>--%>

            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">username</th>
                    <th scope="col">name</th>
                    <th scope="col">designation</th>
                    <th scope="col">attendance</th>
                    <th scope="col">total duration tracked</th>
                </tr>
                </thead>
                <tbody>


                <%



                    while (rs0.next()) {
                        String att = "Absent";

                        long hour = 0;
                        long minute = 0;
                        long second = 0;
                        long baki=0;

                        if(mppDuration.containsKey(rs0.getInt("id")) && mppDuration.get(rs0.getInt("id"))!=null && mppDuration.get(rs0.getInt("id"))>0.0) {
                            att = "Present";
                            baki = mppDuration.get(rs0.getInt("id"))/1000;
                            second = baki % 60;
                            baki/=60;
                            minute = baki%60;
                            baki/=60;
                            hour = baki;
                            baki=0;
                        }

                %>
                <tr>
                    <th scope="row"><%=rs0.getString("username")%></th>
                    <td><%=rs0.getString("name")%></td>
                    <td><%=rs0.getString("designation")%></td>
                    <td><%=att%></td>
                    <td><%=hour+" hour(s), "+minute+" minute(s), "+second+" second(s)"%></td>
                </tr>
                <%}%>
                </tbody>
            </table>
        </div>
        <!-- Large modal -->


        <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="card-body text-center">
                        <h4 class="card-title">Special title treatment</h4>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                    </div>
                    <div class=" card col-8 offset-2 my-2 p-3">
                        <form>
                            <div class="form-group">
                                <label for="listname">List name</label>
                                <input type="text" class="form-control" name="listname" id="listname" placeholder="Enter your listname">
                            </div>
                            <div class="form-group">
                                <label for="datepicker">Deadline</label>
                                <input  type="text" class="form-control" name="datepicker" id="datepicker" placeholder="Pick up a date">
                            </div>
                            <div class="form-group">
                                <label for="datepicker">Add a list item</label>
                                <div class="input-group">

                                    <input type="text" class="form-control" placeholder="Add an item" aria-label="Search for...">
                                    <span class="input-group-btn">
                    <button class="btn btn-secondary" type="button">Go!</button>
                  </span>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-block btn-primary">Sign in</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!---->


<!-- Footer -->
<footer class="text-center">
    <a class="up-arrow" href="#myPage" data-toggle="tooltip" title="TO TOP">
        <span class="glyphicon glyphicon-chevron-up"></span>
    </a><br><br>
    <p>Made By <a href="https://www.w3schools.com" data-toggle="tooltip" title="Visit w3schools">Employee Tracker</a></p>
</footer>
<%} catch (Exception e) {
    System.out.println(e);
} finally {
    db.close();
}

%>
</body>
</html>

