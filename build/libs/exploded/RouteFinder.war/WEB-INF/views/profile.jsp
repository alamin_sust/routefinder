<%@ page import="com.routefinder.web.util.Utils" %>
<%@ page import="com.routefinder.connection.Database" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="javax.management.Query" %><%--
  Created by IntelliJ IDEA.
  User: md_al
  Date: 14-Apr-18
  Time: 4:23 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <!------ Include the above in your HEAD tag ---------->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <%--<script>
        $('button').click(function(){
            $('#pTest').text('test')
        })
    </script>--%>



    <script>
        function showOrHideManager() {
            if(document.getElementsByName("user_type")[0].value === "employee") {
                document.getElementById("manager").style.display = "block";
            } else {
                document.getElementById("manager").style.display = "none";
            }
        }
    </script>



    <style>
        .btn-file {
            position: relative;
            overflow: hidden;
        }
        .btn-file input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            min-width: 100%;
            min-height: 100%;
            font-size: 100px;
            text-align: right;
            filter: alpha(opacity=0);
            opacity: 0;
            outline: none;
            background: white;
            cursor: inherit;
            display: block;
        }
    </style>



    <style>

        body{
            background-image:url("resources/assets/img/backgrounds/1.jpg");/*url(https://static.pexels.com/photos/371633/pexels-photo-371633.jpeg);*/
            background-repeat:no-repeat;
            background-size: 100%;
            font: 400 15px/1.8 Lato, sans-serif;
            color: #69bb5a;
        }



        h3, h4 {
            margin: 10px 0 30px 0;
            letter-spacing: 10px;
            font-size: 20px;
            color: #111;
        }
        .container {
            padding: 80px 120px;
        }
        .person {
            border: 10px solid transparent;
            margin-bottom: 25px;
            width: 80%;
            height: 80%;
            opacity: 0.7;
        }
        .person:hover {
            border-color: #f1f1f1;
        }
        .carousel-inner img {
            -webkit-filter: grayscale(90%);
            filter: grayscale(90%); /* make all photos black and white */
            width: 100%; /* Set width to 100% */
            margin: auto;
        }
        .carousel-caption h3 {
            color: #fff !important;
        }
        @media (max-width: 600px) {
            .carousel-caption {
                display: none; /* Hide the carousel text when the screen is less than 600 pixels wide */
            }
        }
        .bg-1 {
            background: #2d2d30;
            color: #bdbdbd;
        }
        .bg-1 h3 {color: #fff;}
        .bg-1 p {font-style: italic;}
        .list-group-item:first-child {
            border-top-right-radius: 0;
            border-top-left-radius: 0;
        }
        .list-group-item:last-child {
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }
        .thumbnail {
            padding: 0 0 15px 0;
            border: none;
            border-radius: 0;
        }
        .thumbnail p {
            margin-top: 15px;
            color: #555;
        }
        .btn {
            padding: 10px 20px;
            background-color: #333;
            color: #f1f1f1;
            border-radius: 0;
            transition: .2s;
        }
        .btn:hover, .btn:focus {
            border: 1px solid #333;
            background-color: #fff;
            color: #000;
        }
        .modal-header, h4, .close {
            background-color: #333;
            color: #fff !important;
            text-align: center;
            font-size: 30px;
        }
        .modal-header, .modal-body {
            padding: 40px 50px;
        }
        .nav-tabs li a {
            color: #777;
        }
        #googleMap {
            width: 100%;
            height: 400px;
            -webkit-filter: grayscale(100%);
            filter: grayscale(100%);
        }
        .navbar {
            font-family: Montserrat, sans-serif;
            margin-bottom: 0;
            background-color: #2d2d30;
            border: 0;
            font-size: 11px !important;
            letter-spacing: 4px;
            opacity: 0.9;
        }
        .navbar li a, .navbar .navbar-brand {
            color: #d5d5d5 !important;
        }
        .navbar-nav li a:hover {
            color: #fff !important;
        }
        .navbar-nav li.active a {
            color: #fff !important;
            background-color: #29292c !important;
        }
        .navbar-default .navbar-toggle {
            border-color: transparent;
        }
        .open .dropdown-toggle {
            color: #fff;
            background-color: #555 !important;
        }
        .dropdown-menu li a {
            color: #000 !important;
        }
        .dropdown-menu li a:hover {
            background-color: red !important;
        }
        footer {
            background-color: #2d2d30;
            color: #f5f5f5;
            padding: 32px;
        }
        footer a {
            color: #f5f5f5;
        }
        footer a:hover {
            color: #777;
            text-decoration: none;
        }
        .form-control {
            border-radius: 0;
        }
        textarea {
            resize: none;
        }
    </style>
    <link rel="stylesheet" href="resources/table-responsive/css/style.css">
    <title>Dashboard</title>
</head>



<body>
<%
    if(Utils.isEmplyOrNull(session.getAttribute("username"))) {
        response.sendRedirect("login");
    }

    Database db = new Database();
    db.connect();

    try{

        boolean isAdmin = !Utils.isEmplyOrNull(session.getAttribute("userType")) && session.getAttribute("userType").equals("admin");

        String id;
        String q= "select * from user where id=";
        Statement st = db.connection.createStatement();
        ResultSet rs = null;

        String name = "";
        String username = "";
        String password = "";
        String designation = "";
        String details = "";
        String user_type = "";
        String assigned_manager = "";


        if(!Utils.isEmplyOrNull(request.getParameter("id")))
        {
            id=request.getParameter("id");
            q+=id;
            if(!isAdmin && !(!Utils.isEmplyOrNull(session.getAttribute("id"))&&id.equals(session.getAttribute("id")))) {
                q += " and assigned_manager=" + session.getAttribute("id");
            }
            rs = st.executeQuery(q);
            if(!rs.next()) {
                response.sendRedirect("dashboard");
            }

            name = rs.getString("name");
            username = rs.getString("username");
            password = rs.getString("password");
            designation = rs.getString("designation");
            details = rs.getString("details");
            user_type = rs.getString("user_type");
            assigned_manager = rs.getString("assigned_manager");
        } else {
            id= session.getAttribute("id").toString();

        }









%>



<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header navbar-center">

            <a class="navbar-brand" href="dashboard">Dashboard <span class="sr-only">(current)</span></a>
            <a class="navbar-brand" href="history">History <span class="sr-only">(current)</span></a>
            <a class="navbar-brand" href="tracking">Tracking <span class="sr-only">(current)</span></a>
            <a class="navbar-brand" href="profile?id=<%=session.getAttribute("id")%>">Profile <span class="sr-only">(current)</span></a>
            <%if(isAdmin){%><a class="navbar-brand" href="profile">Insert <span class="sr-only">(current)</span></a><%}%>

        </div>
        <div class="navbar-header navbar-right">
            <a class="navbar-brand" href="login">Logout (<%=session.getAttribute("username")%>)</a>
        </div>
    </div>
</nav>







<main>

<div class="container my-5">

    <%if(!Utils.isEmplyOrNull(session.getAttribute("sm"))) {%>
    <div class="alert alert-success">
        <%=session.getAttribute("sm")%>
    </div>
    <%}
        session.setAttribute("sm",null);
    %>

    <%if(!Utils.isEmplyOrNull(session.getAttribute("em"))) {%>
    <div class="alert alert-danger">
        <%=session.getAttribute("em")%>
    </div>
    <%}
        session.setAttribute("em",null);
    %>

        <div class="card-body text-center alert-default">
            <h2 class="card-title">Employee Tracker</h2>
            <h3 class="card-text">User Profile</h3>
        </div>
    <div class="row">


        <div class="col-sm-12" >


            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title"><%=name%></h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" style="width: 250px; height: 250px;" src="resources/images/<%=request.getParameter("id")%>.jpg" class="img-circle img-responsive"> </div>

                        <!--<div class="col-xs-10 col-sm-10 hidden-md hidden-lg"> <br>
                          <dl>
                            <dt>DEPARTMENT:</dt>
                            <dd>Administrator</dd>
                            <dt>HIRE DATE</dt>
                            <dd>11/12/2013</dd>
                            <dt>DATE OF BIRTH</dt>
                               <dd>11/12/2013</dd>
                            <dt>GENDER</dt>
                            <dd>Male</dd>
                          </dl>
                        </div>-->
                        <div class=" col-md-9 col-lg-9 ">
                            <form method="post" action="profile" enctype="multipart/form-data">
                            <table class="table table-user-information">
                                <tbody>











                                <tr>
                                    <td>Photo:</td>
                                    <td><input type="file" class="form-control btn btn-file" name="file" placeholder="photo" ></td>
                                </tr>
                                <tr>
                                    <td>Username:</td>
                                    <td scope="row"><input class="form-control" name="username" placeholder="username" value="<%=username%>" required></td>
                                </tr>
                                <tr>
                                    <td>Password</td>
                                    <td><input type="text" class="form-control" name="password" value="<%=password%>" placeholder="password"></td>
                                </tr>

                                <tr>
                                <tr>
                                    <td>Name</td>
                                    <td><input class="form-control" name="name" value="<%=name%>" placeholder="name"></td>
                                </tr>
                                <tr>
                                    <td>Designation</td>
                                    <td><input class="form-control" name="designation" value="<%=designation%>" placeholder="designation"></td>
                                </tr>
                                <tr>
                                    <td>Details</td>
                                    <td><input class="form-control" name="details" value="<%=details%>" placeholder="details"></td>
                                </tr>


                                <tr>
                                    <td>User Type</td>
                                    <td>
                                        <select name="user_type" class="form-control" value="<%=user_type%>" onchange="javascript:showOrHideManager();" >
                                            <option value="">--Select User Type--</option>
                                            <%if(isAdmin){%><option value="manager">Manager</option><%}%>
                                            <option value="employee">Employee</option>
                                        </select>

                                        <%
                                            String q3 = "select * from user where user_type='manager'";
                                            Statement st3 = db.connection.createStatement();
                                            ResultSet rs3 = st3.executeQuery(q3);
                                        %>



                                    </td>
                                </tr>

                                <tr>
                                    <td>Assigned To</td>
                                    <td>
                                        <select class="form-control" name="manager" value="<%=assigned_manager%>" id="manager" style="display: none;">
                                            <option value="">--Select Assigned to Manager--</option>
                                            <%while (rs3.next()) {%>
                                            <option value="<%=rs3.getString("id")%>"><%=rs3.getString("name")%></option>
                                            <%}%>
                                        </select>
                                    </td>
                                </tr>

                                </tbody>
                            </table>

                                <%if(Utils.isEmplyOrNull(request.getParameter("id"))){%>
                                <input type="hidden" name="insert" value="true">
                                <%}else{%>
                                <input type="hidden" name="updateProductId" value="<%=request.getParameter("id")%>">
                                <%}%>
                                <button type="submit" class="btn btn-sm btn-success" href="#"><i class="fas fa-trash-alt"></i> submit</button>

                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

</main>






<!-- Footer -->
<footer class="text-center">
    <a class="up-arrow" href="#myPage" data-toggle="tooltip" title="TO TOP">
        <span class="glyphicon glyphicon-chevron-up"></span>
    </a><br><br>
    <p>Made By <a href="https://www.w3schools.com" data-toggle="tooltip" title="Visit w3schools">Employee Tracker</a></p>
</footer>
<%} catch (Exception e) {
    System.out.println(e);
    } finally {
    db.close();
    }

%>
</body>
</html>
