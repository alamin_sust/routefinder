<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="com.routefinder.web.util.Utils" %>
<%@ page import="com.routefinder.connection.Database" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="javax.management.Query" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.TreeMap" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %><%--
  Created by IntelliJ IDEA.
  User: md_al
  Date: 14-Apr-18
  Time: 4:23 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Employee Tracker</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="resources/assets/images/favicon.png">
    <!-- Place favicon.ico in the root directory -->
    <!-- all css here -->
    <!-- bootstrap v3.3.7 css -->
    <link rel="stylesheet" href="resources/assets/css/bootstrap.min.css">
    <!-- font-awesome.min.css -->
    <link rel="stylesheet" href="resources/assets/css/font-awesome.min.css">
    <!-- select2.min.css -->
    <link rel="stylesheet" href="resources/assets/css/select2.min.css">
    <!-- style css -->
    <link rel="stylesheet" href="resources/assets/css/styles.css">
    <!-- responsive css -->
    <link rel="stylesheet" href="resources/assets/css/responsive.css">
    <!-- modernizr css -->
    <script src="resources/assets/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
<%
    if(Utils.isEmplyOrNull(session.getAttribute("username"))) {
        response.sendRedirect("login");
    }

    Database db = new Database();
    db.connect();

    try{

        boolean isAdmin = !Utils.isEmplyOrNull(session.getAttribute("userType")) && session.getAttribute("userType").equals("admin");




%>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!-- header-area start -->
<header class="header-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-8 col-12">
                <div class="header-left">
                    <%if(isAdmin){%>
                    <h5><a href="dashboard"><i class="fa fa-home"></i></a> Employee Work Tracking</h5>
                    <%}else{%>
                    <h5><a href="dashboard2"><i class="fa fa-home"></i></a> Employee Work Tracking</h5>
                    <%}%>
                </div>
            </div>
            <div class="col-lg-6 col-md-5 col-sm-3 col-8">
                <div class="header-right text-right">
                    <a href="login"><i class="fa fa-sign-out"></i>Logout</a>
                </div>
            </div>
            <div class="col-md-1 col-sm-1 d-block d-lg-none col-4">
                <ul class="menu">
                    <li class="first"></li>
                    <li class="second"></li>
                    <li class="third"></li>
                </ul>
            </div>
        </div>
    </div>
</header>
<!-- header-area end -->
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-2 col-lg-3 col-12">
            <!-- sidebar-style start -->
            <div class="sidebar-style">
                <div class="sliderbar-area">
                    <h2 class="slidebar-title">Hi <%=session.getAttribute("username")%></h2>
                    <div class="profile-img">
                        <img src="resources/assets/images/<%=session.getAttribute("id")%>.jpg" alt="Profile Picture">
                        <div class="profile-content">
                            <form method="post" action="profile" enctype="multipart/form-data">
                                <input type="hidden" name="uploadImage" value="<%=session.getAttribute("id")%>"/>
                                <input type="file" name="file" required/>
                                <button type="submit"><i class="fa fa-camera"></i>Upload Image</button>
                            </form>
                        </div>
                    </div>
                    <div class="mainmenu">
                        <ul>
                            <%if(isAdmin){%>
                            <li><a href="dashboard">Dashboard</a></li>
                            <li><a href="account-info">Profiles</a></li>
                            <li><a href="account">Accounts</a></li>
                            <li><a href="live-tracking">Live Tracking</a></li>
                            <%}else {%>
                            <li><a href="dashboard2">Dashboard</a></li>
                            <li><a href="account-info">Profiles</a></li>
                            <li ><a href="account">Accounts</a></li>
                            <li><a href="live-tracking2">Live Tracking</a></li>
                            <%}%>
                            <li class="active"><a href="reports">History</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- sidebar-style end -->
        </div>
        <div class="col-xl-10 col-lg-9 col-12">
            <div class="main-content">
                <div class="search-wrapper">

                        <%--<li><h5>Employees</h5></li>--%>
                        <form method="get" action="reports">
                            <div class="row ">
                                <div class="col col-sm-1">
                                    <b>From Date: </b>
                                </div>
                                <div class="col col-sm-3"><input type="date" name="fromDate" placeholder="From Date"
                                                                 class="form-control" required/>
                                </div>
                                <div class="col col-sm-1">
                                    <b>To Date: </b>
                                </div>
                                <div class="col col-sm-3">
                                    <input type="date" name="toDate" placeholder="To Date" class="form-control" required/>
                                </div>
                                <div class="col col-sm-2">
                                    <button type="submit" class="btn btn-primary"> Search</button>
                                </div>
                            </div>
                        </form>

                </div>
                <br>

                <%
                    Statement st0 = db.connection.createStatement();
                    String q0= "select * from user where user_type='employee'";
                    if(!isAdmin){
                        q0+=" and assigned_manager="+session.getAttribute("id");
                    }
                    ResultSet rs0 = st0.executeQuery(q0);



                    String q = "select * from event where id>0 ";

                    String searchDateRange = "Search Date: ";

                    if(Utils.isEmplyOrNull(request.getParameter("fromDate")) || Utils.isEmplyOrNull(request.getParameter("toDate"))) {



                        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
                        Date now = new Date();
                        String strDate = sdfDate.format(now).substring(0,10);
                        searchDateRange +=strDate;
                        q+=" and substr(event_time,1,10) = '"+strDate+"'";
                    } else {
                        searchDateRange += request.getParameter("fromDate")+" to "+request.getParameter("toDate");
                        q+=" and substr(event_time,1,10) >= '"+request.getParameter("fromDate")+"' and substr(event_time,1,10) <= '"
                                +request.getParameter("toDate")+"' ";
                    }



                %>
                <h3><%=searchDateRange%></h3>
                <div class="table-responsive table-style">
                    <table>
                        <thead>
                        <tr>
                            <th >Username</th>
                            <th >Name</th>
                            <th >Designation</th>
                            <th >Attendance</th>
                            <th >Working Hour</th>
                            <th >Break Time</th>
                            <th >Total Kilometers Covered</th>
                        </tr>
                        </thead>
                        <tbody>
                        <%



                            while (rs0.next()) {

                                Statement st = db.connection.createStatement();
                                ResultSet rs = st.executeQuery(q+" and user_id="+rs0.getString("id")+" order by event_time asc");



                                long workingMillis=0;
                                long breakMillis=0;
                                long befMmils=-1;
                                String befLat="-1";
                                String befLong="-1";
                                double distanceCovered = 0.0;



                                while (rs.next()) {
                                    long milis = rs.getTimestamp("event_time").getTime();
                                    String eventType = rs.getString("event_type");

                                    if(befMmils==-1) {
                                        befMmils=milis;
                                        befLat=rs.getString("latitude");
                                        befLong=rs.getString("longitude");
                                        continue;
                                    }
                                    if(eventType.equals("pause") || eventType.equals("end")) {
                                        breakMillis+=milis-befMmils;
                                    } else {
                                        distanceCovered+=Utils.getDistanceInKm(Double.parseDouble(befLat),Double.parseDouble(befLong),
                                                Double.parseDouble(rs.getString("latitude")),Double.parseDouble(rs.getString("longitude")),0.0,0.0);
                                        workingMillis+=milis-befMmils;
                                    }
                                    befMmils=milis;
                                    befLat=rs.getString("latitude");
                                    befLong=rs.getString("longitude");
                                }






                                String att = "Absent";

                                if(workingMillis>0){
                                    att = "Present";
                                }

                                long hour = 0;
                                long minute = 0;
                                long second = 0;
                                long baki=0;

                                baki = workingMillis/1000;
                                second = baki % 60;
                                baki/=60;
                                minute = baki%60;
                                baki/=60;
                                hour = baki;
                                baki=0;

                                long bhour = 0;
                                long bminute = 0;
                                long bsecond = 0;
                                long bbaki=0;

                                bbaki = breakMillis/1000;
                                bsecond = bbaki % 60;
                                bbaki/=60;
                                bminute = bbaki%60;
                                bbaki/=60;
                                bhour = bbaki;
                                bbaki=0;

                                distanceCovered*=100.0;
                                distanceCovered = (double) ((long)distanceCovered) /100.0;

                        %>
                        <tr>
                            <th ><%=rs0.getString("username")%></th>
                            <td><%=rs0.getString("name")%></td>
                            <td><%=rs0.getString("designation")%></td>
                            <td><%=att%></td>
                            <td><%=hour+" hrs, "+minute+" mins, "+second+" secs"%></td>
                            <td><%=bhour+" hrs, "+bminute+" mins, "+bsecond+" secs"%></td>
                            <td><%=distanceCovered%> km</td>
                        </tr>
                        <%}%>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- jquery latest version -->
<script src="resources/assets/js/vendor/jquery-2.2.4.min.js"></script>
<!-- bootstrap js -->
<script src="resources/assets/js/bootstrap.min.js"></script>
<!-- waypoints.min.js -->
<script src="resources/assets/js/waypoints.min.js"></script>
<!-- jquery.counterup.min.js -->
<script src="resources/assets/js/jquery.counterup.min.js"></script>
<!-- select2.min.js -->
<script src="resources/assets/js/select2.min.js"></script>
<!-- main js -->
<script src="resources/assets/js/scripts.js"></script>
<%} catch (Exception e) {
    System.out.println(e);
} finally {
    db.close();
}

%>
</body>

</html>