package com.routefinder.web.controller;

import com.google.maps.errors.ApiException;
import com.routefinder.service.AppAuthService;
import com.routefinder.web.util.Node;
import com.routefinder.web.util.Utils;
import org.apache.commons.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

@Controller
public class HomeController {

    @Autowired
    private AppAuthService appAuthService;

    @GetMapping("/")
    public String mainGet() {
        return "login";
    }

    @GetMapping("/tracking")
    public String trackingGet() {
        return "tracking";
    }

    @PostMapping("/tracking")
    public String trackingPost() {
        return "tracking";
    }

    @GetMapping("/routeMap")
    public String routeMapGet() {
        return "routeMap";
    }

    @PostMapping("/routeMap")
    public String routeMapPost(HttpServletRequest request, HttpSession session) throws FileUploadException, SQLException, IOException {
        return "routeMap";
    }

    @GetMapping("/routeMapResult")
    public String routeMapResultGet() {
        return "routeMapResult";
    }

    @PostMapping("/routeMapResult")
    public String routeMapResultPost(HttpServletRequest request, HttpSession session) throws FileUploadException, SQLException, IOException, ApiException, InterruptedException {
        String sourceAddress = appAuthService.processMultipartRequest(request,session);
        List<String> destinationList = Utils.getLocationsFromExcel();
        List<String> locList = new LinkedList<>();
        locList.add(sourceAddress);
        locList.addAll(destinationList);
        List<Node>[] actualLocList = Utils.findOptimalRouteFromLatLong(Utils.getLocationLatLong(sourceAddress, destinationList),locList);

        if(actualLocList==null) {
            session.setAttribute("errMsg", "Can't Be delivered within time!");
            return "routeMap";
        }

        Utils.updateDBWithNewestRoutes(actualLocList);
        Utils.updateDBWithNewestLocStrs(locList);
        return "routeMapResult";
    }

    @GetMapping("/direction")
    public String directionGet() {
        return "direction";
    }

    @PostMapping("/direction")
    public String directionPost() {
        return "direction";
    }

    @GetMapping("/live-tracking")
    public String lTGet() {
        return "live-tracking";
    }

    @PostMapping("/live-tracking")
    public String lTPost() {
        return "live-tracking";
    }

    @GetMapping("/live-tracking2")
    public String lT2Get() {
        return "live-tracking2";
    }

    @PostMapping("/live-tracking2")
    public String lT2Post() {
        return "live-tracking2";
    }
}
