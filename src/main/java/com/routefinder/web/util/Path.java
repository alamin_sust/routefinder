package com.routefinder.web.util;

/**
 * Created by md_al on 27-May-18.
 */
public class Path {
    private int index;
    private Long distCovered;
    private Long durationElapsed;
    private int currentNode;

    public Path(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Long getDistCovered() {
        return distCovered;
    }

    public void setDistCovered(Long distCovered) {
        this.distCovered = distCovered;
    }

    public Long getDurationElapsed() {
        return durationElapsed;
    }

    public void setDurationElapsed(Long durationElapsed) {
        this.durationElapsed = durationElapsed;
    }

    public int getCurrentNode() {
        return currentNode;
    }

    public void setCurrentNode(int currentNode) {
        this.currentNode = currentNode;
    }
}
