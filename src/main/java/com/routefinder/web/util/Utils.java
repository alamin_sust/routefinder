package com.routefinder.web.util;


import com.google.maps.*;
import com.google.maps.errors.ApiException;
import com.google.maps.model.*;
import com.routefinder.connection.Database;
import com.sun.org.apache.regexp.internal.RE;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;

public class Utils {

    public static Long[][][] distanceMatrix(String[] origins, String[] destinations) throws ApiException, InterruptedException, IOException{
        GeoApiContext context = new GeoApiContext.Builder()
                .apiKey("AIzaSyD3k0yRShRxxCaaL2U0LU2Jq3GxDRj9PeE")
                .build();

        DistanceMatrixApiRequest req= DistanceMatrixApi.newRequest(context);
        DistanceMatrix t=req.origins(origins).destinations(destinations).mode(TravelMode.DRIVING).await();
        //long[][] array=new long[origins.length][destinations.length];
        //File file=new File("Matrix.txt");
        //FileOutputStream out=new FileOutputStream(file);
        //DataOutputStream outFile=new DataOutputStream(out);
        Long[][][] mat =  new Long[origins.length][origins.length][2];
        for(int i=0;i<origins.length;i++){
            for(int j=0;j<destinations.length;j++){
                System.out.println(t.rows[i].elements[j].distance.inMeters);
                mat[i][j][0]=t.rows[i].elements[j].distance.inMeters;
                mat[i][j][1]=t.rows[i].elements[j].duration.inSeconds;
                //outFile.writeLong(t.rows[i].elements[j].distance.inMeters);
            }
        }
        //outFile.close();
        return mat;
    }

    public static Double[][] getLocationLatLong(String sourceAddress, List<String> destinationList) throws InterruptedException, ApiException, IOException {
        List<String> locList = new LinkedList<>();
        locList.add(sourceAddress);
        locList.addAll(destinationList);
        String[] locs = new String[locList.size()];
        for(int i=0;i<locList.size();i++) {
            locs[i] = locList.get(i);
        }

        Double[][] coords = new Double[locs.length][2];
        for(int i=0;i<locs.length;i++) {
            GeoApiContext context = new GeoApiContext.Builder()
                    .apiKey("AIzaSyD3k0yRShRxxCaaL2U0LU2Jq3GxDRj9PeE")
                    .build();

            GeocodingResult[] results = GeocodingApi.geocode(context,
                    locs[i]).await();
            LatLng coord = (results[0].geometry.location);
            coords[i][0] = coord.lat;
            coords[i][1] = coord.lng;
        }
        return coords;
    }

    public static Long[][][] generateDistanceMatrix(String sourceAddress, List<String> destinationList) throws InterruptedException, ApiException, IOException {
        List<String> locList = new LinkedList<>();
        locList.add(sourceAddress);
        locList.addAll(destinationList);
        String[] locs = new String[locList.size()];
        for(int i=0;i<locList.size();i++) {
            locs[i] = locList.get(i);
        }
        return distanceMatrix(locs,locs);
    }


    public static List<String> getLocationsFromExcel() {
        List<String> locationList = new ArrayList<>();
        try {
            File file = (new File("C:\\Users\\md_al\\Documents\\NetBeansProjects\\RouteFinder\\src\\main\\resources\\Excels\\testLocations.xls"));

            POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(file));
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            HSSFSheet sheet = wb.getSheetAt(0);
            HSSFRow row;
            HSSFCell cell;

            int rows; // No of rows
            rows = sheet.getPhysicalNumberOfRows();

            int cols = 0; // No of columns
            int tmp = 0;

            // This trick ensures that we get the data properly even if it doesn't start from first few rows
            for(int i = 0; i < 10 || i < rows; i++) {
                row = sheet.getRow(i);
                if(row != null) {
                    tmp = sheet.getRow(i).getPhysicalNumberOfCells();
                    if(tmp > cols) cols = tmp;
                }
            }

            for(int r = 0; r < rows; r++) {
                row = sheet.getRow(r);
                if(row != null) {
                    for(int c = 0; c < cols; c++) {
                        cell = row.getCell((short)c);
                        if(cell != null) {
                            // Your code here
                            locationList.add(cell.getStringCellValue());
                        }
                    }
                }
            }
        } catch(Exception ioe) {
            ioe.printStackTrace();
        }
        return locationList;
    }

    public static boolean isEmplyOrNull(Object string) {
        return string == null || string.toString().equals("");
    }

    public static String getTimeStrNow() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        Date date = new Date();
        return simpleDateFormat.format(date);
    }

    public static double getDistanceInKm(double lat1,double lon1,
                                         double lat2, double lon2, double el1, double el2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance)/1000.0;
    }

    public static List<Node>[] findOptimalRouteFromLatLong(Double[][] locationLatLong, List<String> locList) throws SQLException, InterruptedException, ApiException, IOException {
        Long perTime = 0L;
        Long totalTime = 0L;

        Database db = new Database();
        db.connect();
        try {
            Statement st = db.connection.createStatement();
            String q= "SELECT * from current";
            ResultSet rs = st.executeQuery(q);
            rs.next();
            perTime = rs.getLong("per_time");
            totalTime = rs.getLong("total_time");
        } catch (Exception e) {
            System.out.println(e);
        }finally {
            db.close();
        }

        int highBoysNeeded =20;
        boolean gFlag = false;
        List<Node>[] actualLocList = new ArrayList[highBoysNeeded];

            Queue<Path> priorityQueue = new PriorityQueue<>(highBoysNeeded, durationComparator);

            for(int i=0;i<highBoysNeeded;i++) {
                Path path = new Path(i);
                path.setDurationElapsed(0L);
                path.setCurrentNode(0);
                priorityQueue.add(path);
            }

            boolean flag=true;
            Map<Integer,Boolean> visited = new HashMap<>();
            visited.put(0,true);

            List<Node>[] currLocList = new ArrayList[highBoysNeeded];
            for (int i=1;i<locList.size();i++) {
                Path path = priorityQueue.poll();
                int to = getClosestByDistanceLatLong(locationLatLong, path.getCurrentNode(), visited, locList.size());
                Long duration = getDurationFromAPI(locList.get(path.getCurrentNode()),locList.get(to));
                Double distance = getLinearDistance(locationLatLong[path.getCurrentNode()][0],locationLatLong[path.getCurrentNode()][1],
                        locationLatLong[to][0],locationLatLong[to][1]);

                if((path.getDurationElapsed()+duration+perTime)>totalTime*60) {
                    if((path.getIndex()+1)==highBoysNeeded)
                    {
                        flag = false;
                        break;
                    } else {
                        i--;
                        continue;
                    }
                }
                path.setDurationElapsed(path.getDurationElapsed()+duration+perTime);
                path.setCurrentNode(to);

                if(currLocList[path.getIndex()]==null || currLocList[path.getIndex()].size() == 0) {
                    currLocList[path.getIndex()] = new ArrayList<>();
                    currLocList[path.getIndex()].add(new Node(0,-1,0.0,0L));
                }
                int parent =currLocList[path.getIndex()].get(currLocList[path.getIndex()].size()-1).node;
                currLocList[path.getIndex()].add(new Node(to,
                        parent,
                        distance,
                        duration));
                visited.put(to,true);
                priorityQueue.add(path);
            }
            if(flag) {
                int actualBoysNeeded = priorityQueue.poll().getIndex()+1;
                gFlag=true;
                for (int i=0;i<actualBoysNeeded;i++) {
                    actualLocList[i] = new ArrayList<>();

                }
                for (int i=0;i<actualBoysNeeded;i++) {
                    actualLocList[i] = new ArrayList<>();
                    if(currLocList[i]!=null) {
                        actualLocList[i].addAll(currLocList[i]);
                    }
                }

            }
        return gFlag==false?null:actualLocList;
    }

    private static Long getDurationFromAPI(String from, String to) throws InterruptedException, ApiException, IOException {

        GeoApiContext context = new GeoApiContext.Builder()
                .apiKey("AIzaSyD3k0yRShRxxCaaL2U0LU2Jq3GxDRj9PeE")
                .build();

        DirectionsResult result = DirectionsApi.newRequest(context)
                .origin(from)
                .destination(to)
                .mode(TravelMode.DRIVING)
                .await();


        return result.routes == null || result.routes.length == 0 ? 1000000L : result.routes[0].legs[0].duration.inSeconds;

    }

    private static Double getLinearDistance(double lat1,double lon1,double lat2,double lon2) {
        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        //double height = el1 - el2;

        distance = Math.pow(distance, 2);//+ Math.pow(height, 2);

        return Math.sqrt(distance);
    }

    private static int getClosestByDistanceLatLong(Double[][] locationLatLong, int currentNode, Map<Integer, Boolean> visited, int totalNodes) {

        int ret = 0;
        double dist = 1000000000L;
        for(int i=0;i<totalNodes;i++) {
            if(visited.containsKey(i)) {
                continue;
            }
            Double currDist = getLinearDistance(locationLatLong[currentNode][0],locationLatLong[currentNode][1],locationLatLong[i][0],locationLatLong[i][1]);
            if(dist>currDist) {
                dist = currDist;
                ret = i;
            }
        }
        return ret;

    }

    /*public static List<Node>[] findOptimalRoute(Long[][][] mat, List<String> locList) throws SQLException {

        Long perTime = 0L;
        Long totalTime = 0L;

        Database db = new Database();
        db.connect();
        try {
            Statement st = db.connection.createStatement();
            String q= "SELECT * from current";
            ResultSet rs = st.executeQuery(q);
            rs.next();
            perTime = rs.getLong("per_time");
            totalTime = rs.getLong("total_time");
        } catch (Exception e) {
            System.out.println(e);
        }finally {
            db.close();
        }

        int lowBoysNeeded =1;
        int highBoysNeeded =1000;
        int actualBoysNeeded = highBoysNeeded;
        boolean gFlag = false;
        List<Node>[] actualLocList = new ArrayList[actualBoysNeeded];

        while (lowBoysNeeded<=highBoysNeeded) {
            int midBoysNeeded = (lowBoysNeeded+highBoysNeeded)/2;
            Queue<Path> priorityQueue = new PriorityQueue<>(midBoysNeeded, durationComparator);

            for(int i=0;i<midBoysNeeded;i++) {
                Path path = new Path(i);
                path.setDurationElapsed(0L);
                path.setCurrentNode(0);
                priorityQueue.add(path);
            }

            boolean flag=true;
            Map<Integer,Boolean> visited = new HashMap<>();
            visited.put(0,true);

            List<Node>[] currLocList = new ArrayList[midBoysNeeded];
            for (int i=1;i<locList.size();i++) {
                Path path = priorityQueue.poll();
                int to = getClosestByDuration(mat, path.getCurrentNode(), visited, locList.size());
                path.setDurationElapsed(path.getDurationElapsed()+mat[path.getCurrentNode()][to][1]+perTime);
                if(path.getDurationElapsed()>totalTime*60) {
                    flag = false;
                    break;
                }
                if(currLocList[path.getIndex()]==null || currLocList[path.getIndex()].size() == 0) {
                    currLocList[path.getIndex()] = new ArrayList<>();
                    currLocList[path.getIndex()].add(new Node(0,-1,0L,0L));
                }
                int parent =currLocList[path.getIndex()].get(currLocList[path.getIndex()].size()-1).node;
                currLocList[path.getIndex()].add(new Node(to,
                        parent,
                        mat[parent][to][0],
                        mat[parent][to][1]));
                visited.put(to,true);
                priorityQueue.add(path);
            }
            if(flag) {
                gFlag=true;
                for (int i=0;i<actualBoysNeeded;i++) {
                    actualLocList[i] = new ArrayList<>();
                    actualLocList[i].clear();
                }
                for (int i=0;i<midBoysNeeded;i++) {
                    actualLocList[i] = new ArrayList<>();
                    if(currLocList[i]!=null) {
                        actualLocList[i].addAll(currLocList[i]);
                    }
                }
                actualBoysNeeded = midBoysNeeded;
                highBoysNeeded = midBoysNeeded - 1;
            } else {
                lowBoysNeeded = midBoysNeeded + 1;
            }
        }
        return gFlag==false?null:actualLocList;
    }*/

    private static int getClosestByDuration(Long[][][] mat, int currentNode, Map<Integer, Boolean> visited, int totalNodes) {
        int ret = 0;
        Long dur = 1000000000L;
        for(int i=0;i<totalNodes;i++) {
            if(visited.containsKey(i)) {
                continue;
            }
            if(dur>mat[currentNode][i][1]) {
                dur = mat[currentNode][i][1];
                ret = i;
            }
        }
        return ret;
    }


    //Comparator anonymous class implementation
    public static Comparator<Path> durationComparator = new Comparator<Path>(){

        @Override
        public int compare(Path c1, Path c2) {
            return (int) (c1.getIndex() - c2.getIndex());
        }
    };

    public static void updateDBWithNewestRoutes(List<Node>[] actualLocList) throws SQLException {

        Database db = new Database();
        db.connect();
        try {
            Statement st = db.connection.createStatement();
            String q = "DELETE from location";
            st.executeUpdate(q);

            for(int i=0;i<actualLocList.length;i++) {
                Statement st2 = db.connection.createStatement();

                String str = "";
                List<Node> nodeList = actualLocList[i];

                if(nodeList==null || nodeList.size()==0) {
                    break;
                }

                for(int j=0;j<nodeList.size();j++) {
                    Node node = nodeList.get(j);
                    if(!str.equals("")) {
                        str+=":";
                    }
                    str += node.getNode() + "_" + node.getParent() + "_" + node.getDistanceFromParent() + "_" + node.getDurationFromParent();
                }


                String q2 = "insert into location (boy_id, loc_str) values ("+i+",'"+str+"')";
                st2.executeUpdate(q2);
            }
        } catch (Exception e) {
            System.out.println(e);
        }finally {
            db.close();
        }
    }

    public static void updateDBWithNewestLocStrs(List<String> locList) throws SQLException {
        Database db = new Database();
        db.connect();
        try {
            Statement st = db.connection.createStatement();
            String q = "DELETE from address";
            st.executeUpdate(q);
            for(int i=0;i<locList.size();i++) {
                Statement st2 = db.connection.createStatement();
                String q2 = "insert into address (id,address)  VALUES ("+i+",'"+locList.get(i)+"')";
                st2.executeUpdate(q2);
            }
        } catch (Exception e) {
            System.out.println(e);
        }finally {
            db.close();
        }
    }







/*
    private static ResultSet executeQuery(String q) throws SQLException {

        ResultSet rs = null;
        Database db = new Database();
        db.connect();
        try {
            Statement st = db.connection.createStatement();
            rs = st.executeQuery(q);
        } catch (Exception e) {
            System.out.println(e);
        }finally {
            //db.close();
        }
        return rs;
    }

    public static int getTotalOnline() throws SQLException {
        ResultSet rs = executeQuery("select distinct user_id as cnt from event where substr(event_time,1,16)>='"+ Utils.getTimeStrNow()+"'");
        rs.next();
        return rs.getInt("cnt");
    }

    public static int getTotalOnline(String managerId) throws SQLException {
        ResultSet rs = executeQuery("select distinct user_id as cnt from event where substr(event_time,1,16)>="+ Utils.getTimeStrNow());
        rs.next();
        return rs.getInt("cnt");
    }

    public static int getTotalManagers() throws SQLException {
        ResultSet rs =  executeQuery("select count(*) as cnt from user where user_type='manager'");
        rs.next();
        return rs.getInt("cnt");
    }

    public static int getTotalEmployees() throws SQLException {
        ResultSet rs = executeQuery("select count(*) as cnt from user where user_type='employee'");
        rs.next();
        return rs.getInt("cnt");
    }

    public static ResultSet getEmployees(String managerId) throws SQLException {
        return executeQuery("select * from user where  user_type='employee' and assigned_manager="+managerId);
    }

    public static ResultSet getManagers() throws SQLException {
        return executeQuery("select * from user where  user_type='manager'");
    }
*/


}
