package com.routefinder.web.util;

/**
 * Created by md_al on 27-May-18.
 */
public class Node {
    int node;
    int parent;
    Double distanceFromParent;
    Long durationFromParent;

    public Node(int node, int parent, Double distanceFromParent, Long durationFromParent) {
        this.node = node;
        this.parent = parent;
        this.distanceFromParent = distanceFromParent;
        this.durationFromParent = durationFromParent;
    }

    public int getNode() {
        return node;
    }

    public void setNode(int node) {
        this.node = node;
    }

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }

    public Double getDistanceFromParent() {
        return distanceFromParent;
    }

    public void setDistanceFromParent(Double distanceFromParent) {
        this.distanceFromParent = distanceFromParent;
    }

    public Long getDurationFromParent() {
        return durationFromParent;
    }

    public void setDurationFromParent(Long durationFromParent) {
        this.durationFromParent = durationFromParent;
    }
}
