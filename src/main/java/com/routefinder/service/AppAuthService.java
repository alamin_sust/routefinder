package com.routefinder.service;

import com.routefinder.connection.Database;
import com.routefinder.web.util.Utils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Al-Amin on 3/11/2017.
 */
@Service
public class AppAuthService {



    public String processMultipartRequest(HttpServletRequest request, HttpSession session) throws FileUploadException, IOException, SQLException {

        Database db = new Database();
        String ret = "";
        db.connect();
        try {

            List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);

            List<InputStream> fileContentList = new LinkedList<>();
            boolean hasExcel = false;

            String perTime = "";
            String startingAddress = "";
            String totalTime = "";

            for (FileItem item : items) {
                if (item.isFormField()) {
                    String fieldname = item.getFieldName();
                    String fieldvalue = item.getString().replace("'","\\'");
                    if (fieldname.equals("perTime")) {
                        perTime= fieldvalue;
                    } else if (fieldname.equals("startingAddress")) {
                        startingAddress= fieldvalue;
                        ret = startingAddress;
                    } else if (fieldname.equals("totalTime")) {
                        totalTime= fieldvalue;
                    }
                } else {
                    String fieldname = item.getFieldName();
                    if (fieldname.startsWith("file")) {
                        fileContentList.add(item.getInputStream());
                        hasExcel = true;
                    }
                }
            }

            Statement st = db.connection.createStatement();
            String q  = "update current set per_time='"+perTime
                    +"', starting_address='"+startingAddress+"', total_time="+totalTime
                    +" where id=1";
            st.executeUpdate(q);

            if (hasExcel) {

                for (int i = 0; i < fileContentList.size(); i++) {

                    InputStream fileContent = fileContentList.get(i);

                    File bfile = new File("C:\\Users\\md_al\\Documents\\NetBeansProjects\\RouteFinder\\src\\main\\resources\\Excels\\testLocations.xls");
                    OutputStream outStream = new FileOutputStream(bfile);

                    byte[] buffer = new byte[1024];
                    int length;
                    while ((length = fileContent.read(buffer)) > 0) {
                        outStream.write(buffer, 0, length);
                    }
                    fileContent.close();
                    outStream.close();
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            db.close();
        }
        return ret;
    }
}
