<%@ page import="com.routefinder.connection.Database" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.sql.Array" %><%--
  Created by IntelliJ IDEA.
  User: md_al
  Date: 26-May-18
  Time: 4:27 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Waypoints in directions</title>
    <style>
        #right-panel {
            font-family: 'Roboto','sans-serif';
            line-height: 30px;
            padding-left: 10px;
        }

        #right-panel select, #right-panel input {
            font-size: 15px;
        }

        #right-panel select {
            width: 100%;
        }

        #right-panel i {
            font-size: 12px;
        }
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        #map {
            float: left;
            width: 75%;
            height: 70%;
        }
        #right-panel {
            margin: 20px;
            border-width: 2px;
            width: 20%;
            height: 400px;
            float: left;
            text-align: left;
            padding-top: 0;
        }
        #down-panel {
            border-width: 2px;
            width: 100%;
            height: 400px;
            float: left;
            text-align: left;
            padding-top: 0;
        }
        #directions-panel {
            margin-top: 10px;
            background-color: #FFEE77;
            padding: 10px;
            overflow: scroll;
            height: 174px;
        }

        #directions-panel2 {
            margin-top: 10px;
            background-color: #FFEE77;
            padding: 10px;
            overflow: scroll;
            height: 174px;
        }




        /*Contact sectiom*/
        .content-header{
            font-family: 'Oleo Script', cursive;
            color:#fcc500;
            font-size: 45px;
        }

        .section-content{
            text-align: center;

        }
        #contact{

            font-family: 'Teko', sans-serif;
            padding-top: 60px;
            padding-bottom: 30px;
            width: 100%;
            background: #3a6186; /* fallback for old browsers */
            background: -webkit-linear-gradient(to left, #3a6186 , #89253e); /* Chrome 10-25, Safari 5.1-6 */
            background: linear-gradient(to left, #3a6186 , #89253e); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
            color : #fff;
        }
        .contact-section{
            padding-top: 40px;
        }
        .contact-section .col-md-6{
            width: 50%;
        }

        .form-line{
            border-right: 1px solid #B29999;
        }

        .form-group{
            margin-top: 10px;
        }
        label{
            font-size: 1.3em;
            line-height: 1em;
            font-weight: normal;
        }
        .form-control{
            font-size: 1.3em;
            color: #080808;
        }
        textarea.form-control {
            height: 135px;
            /* margin-top: px;*/
        }

        .submit{
            font-size: 1.1em;
            float: right;
            width: 150px;
            background-color: transparent;
            color: #fff;

        }
    </style>
</head>
<body>
<%

    Database db = new Database();
    db.connect();
    try {
        Statement st = db.connection.createStatement();
        String q = "select * from address";
        ResultSet addressRs = st.executeQuery(q);

        ArrayList<String> addressList = new ArrayList<>();
        while (addressRs.next()) {
            addressList.add(addressRs.getString("address"));
        }

        Statement st2 = db.connection.createStatement();
        String q2 = "select * from location";
        ResultSet locationRs = st2.executeQuery(q2);

        int boysNeeded = 0;

        while (locationRs.next()) {
            boysNeeded++;
        }
        locationRs.beforeFirst();

        Statement st3 = db.connection.createStatement();
        String q3 = "select * from CURRENT ";
        ResultSet qRs = st3.executeQuery(q3);
        qRs.next();

        String perTime = qRs.getString("per_time");
        String totalTime = qRs.getString("total_time");


        ArrayList<String>[] boysRoute = new ArrayList[boysNeeded];
        Integer[] boysTime = new Integer[boysNeeded];
        boysNeeded=0;
        while (locationRs.next()) {
            List<String> boyRoute = new ArrayList<>();
            boysTime[boysNeeded]=0;
            String[] locList = locationRs.getString("loc_str").split(":");
            for(int i=0;i<locList.length;i++) {
                if(locList[i]==null) {
                    continue;
                }
                boyRoute.add(addressList.get(Integer.valueOf(locList[i].split("_")[0])));
                boysTime[boysNeeded]+=Integer.valueOf(locList[i].split("_")[3]);
            }
            boysTime[boysNeeded]+=(locList.length * Integer.valueOf(perTime));
            boysRoute[boysNeeded] = new ArrayList<>();
            boysRoute[boysNeeded].addAll(boyRoute);
            boysNeeded++;
        }







%>
<div id="map"></div>
<div id="right-panel">
    <div>
        <%
            int id = 0;
            int part = 0;
            int maxElementPerPart = 25;

            if(request.getParameter("id")!=null && !request.getParameter("id").equals(""))
            {
                id = Integer.parseInt(request.getParameter("id"));
            }
            int parts = (boysRoute[id].size()-1)/(maxElementPerPart-1) + ((boysRoute[id].size()-1)%(maxElementPerPart-1)==0?0:1);
            if(request.getParameter("part")!=null && !request.getParameter("part").equals(""))
            {
                part = Math.min(Integer.parseInt(request.getParameter("part")), parts-1);
            }

            int startId = (part*maxElementPerPart-part);
            int endId = Math.min(startId + maxElementPerPart - 1, boysRoute[id].size()-1);


        %>

        <b>Start:</b>
        <select id="start">
            <option value="<%=boysRoute[id].get(startId)%>" selected><%=boysRoute[id].get(startId)%></option>
        </select>
        <br>
        <b>Waypoints:</b> <br>
        <select multiple id="waypoints">
            <%for(int i=startId+1;i<endId;i++){%>
            <option value="<%=boysRoute[id].get(i)%>" selected><%=boysRoute[id].get(i)%></option>
            <%}%>
        </select>
        <br>
        <b>End:</b>
        <select id="end">
            <option value="<%=boysRoute[id].get(endId)%>" selected><%=boysRoute[id].get(endId)%></option>
        </select>
        <br>
        <input type="hidden" name="id" value="<%=id%>">
        <input type="submit" id="submit" value="GET DIRECTION">
    </div>
    <div class="row"><button class="btn btn-primary" onclick="window.location.href='routeMap'">Back to Training Page</button></div>
    <div id="directions-panel"></div>

</div>
<div id="down-panel">
<section id="contact">
    <div class="section-content">
        <h3>Routes</h3>
        <h3>Boy No. : <%=(id+1)%></h3>
        <h3>Path Part Number: (<%=part+1%>/<%=parts%>)</h3>
        <h3>Total Path Duration : <%="" + (boysTime[id]/60)+" minutes "+(boysTime[id]%60)+" seconds"%></h3>
        <h3>Max Delivery Duration : <%=totalTime%> Minutes</h3>
        <h3>Delivery Boys Needed: <%=boysNeeded%></h3>
        <h3>Per Delivery Time: <%=perTime%> seconds</h3>
        <form method="get" action="routeMapResult">
            <select class="form-control" name="id" required>
            <option value="">--Select Delivery Boy--</option>
            <%
                int maxRows = 0;
                for(int i=0;i<boysNeeded;i++) {
                    maxRows = Math.max(maxRows,boysRoute[i].size());
            %>
            <option value="<%=i%>">Delivery Boy <%=(i+1)%></option>
            <%}%>
            </select>
            <select class="form-control" name="part" required>
            <option value="">--Select Path Part Number--</option>
            <%

                for(int i=0;i<parts;i++) {

            %>
            <option value="<%=i%>">Part <%=(i+1)%></option>
            <%}%>
            </select>
            <input class="btn btn-success" type="submit" value="Get Route"/>
        </form>
        <h3>Delivery Boy <%=(id+1)%>'s Locations:</h3>
        <%
            for(int i=0;i<boysRoute[id].size();i++) {
        %>
        <p><%=i+1%>) <%=boysRoute[id].get(i)%></p>
        <%}%>

    </div>
    <div class="contact-section">
        <div class="container">

            <div class="col-md-12">
                <table class="table table-responsive table-bordered">
                    <tr class="rounded-circle">

                    </tr>
                    <%--<%for(int i=0;i<maxRows;i++) {%>
                    <tr >
                        <%for(int j=0;j<boysNeeded;j++) {
                        %>
                        <td><%=boysRoute[j].size()>i?boysRoute[j].get(i):""%></td>
                        <%}%>
                    </tr>

                    <%}%>--%>
                </table>

            </div>
        </div>
    </div>
</section>
</div>

<script>
    function initMap() {
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 3,
            center: {lat: 41.85, lng: -87.65}
        });
        directionsDisplay.setMap(map);

        document.getElementById('submit').addEventListener('click', function() {
            calculateAndDisplayRoute(directionsService, directionsDisplay);
        });
    }

    function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        var waypts = [];
        var checkboxArray = document.getElementById('waypoints');
        for (var i = 0; i < checkboxArray.length; i++) {
            if (checkboxArray.options[i].selected) {
                waypts.push({
                    location: checkboxArray[i].value,
                    stopover: true
                });
            }
        }

        directionsService.route({
            origin: document.getElementById('start').value,
            destination: document.getElementById('end').value,
            waypoints: waypts,
            optimizeWaypoints: true,
            travelMode: 'DRIVING'
        }, function(response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);
                var route = response.routes[0];
                var summaryPanel = document.getElementById('directions-panel');
                summaryPanel.innerHTML = '';
                // For each route, display summary information.
                for (var i = 0; i < route.legs.length; i++) {
                    var routeSegment = i + 1;
                    summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment
                        + '</b><br><br>Time:' + route.legs[i].duration.text +
                        '<br>';
                    summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                    summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                    summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
                }
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD3k0yRShRxxCaaL2U0LU2Jq3GxDRj9PeE&callback=initMap">
</script>
<%} catch (Exception e) {
    System.out.println(e);
} finally {
    db.close();
}

%>
</body>
</html>