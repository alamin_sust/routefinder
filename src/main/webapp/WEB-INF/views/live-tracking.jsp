<%--
  Created by IntelliJ IDEA.
  User: md_al
  Date: 21-May-18
  Time: 12:56 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- all css here -->
    <!-- bootstrap v3.3.7 css -->
    <link rel="stylesheet" href="resources/assets/css/bootstrap.min.css">
    <!-- metisMenu.min.css -->
    <link rel="stylesheet" href="resources/assets/css/metisMenu.min.css">
    <!-- font-awesome.min.css -->
    <link rel="stylesheet" href="resources/assets/css/font-awesome.min.css">
    <!-- select2.min.css -->
    <link rel="stylesheet" href="resources/assets/css/select2.min.css">
    <!-- style css -->
    <link rel="stylesheet" href="resources/assets/css/styles.css">
    <!-- responsive css -->
    <link rel="stylesheet" href="resources/assets/css/responsive.css">
    <!-- modernizr css -->
    <script src="resources/assets/js/vendor/modernizr-2.8.3.min.js"></script>
    <title>Title</title>
    <style>

        /*Contact sectiom*/
        .content-header{
            font-family: 'Oleo Script', cursive;
            color:#fcc500;
            font-size: 45px;
        }

        .section-content{
            text-align: center;

        }
        #contact{

            font-family: 'Teko', sans-serif;
            padding-top: 60px;
            width: 100%;
            width: 100vw;
            background: #3a6186; /* fallback for old browsers */
            background: -webkit-linear-gradient(to left, #3a6186 , #89253e); /* Chrome 10-25, Safari 5.1-6 */
            background: linear-gradient(to left, #3a6186 , #89253e); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
            color : #fff;
        }
        .contact-section{
            padding-top: 40px;
        }
        .contact-section .col-md-6{
            width: 50%;
        }

        .form-line{
            border-right: 1px solid #B29999;
        }

        .form-group{
            margin-top: 10px;
        }
        label{
            font-size: 1.3em;
            line-height: 1em;
            font-weight: normal;
        }
        .form-control{
            font-size: 1.3em;
            color: #080808;
        }
        textarea.form-control {
            height: 135px;
            /* margin-top: px;*/
        }

        .submit{
            font-size: 1.1em;
            float: right;
            width: 150px;
            background-color: transparent;
            color: #fff;

        }

    </style>
    <script>
        var rowId = 0;

        function increaseRowId() {
            rowId++;
        }
        function decreaseRowId() {
            rowId--;
        }
        function resetButton() {
            if(rowId<=0) {
                document.getElementById("destinationAddressRemoveButton").style.display = "none";
            } else {
                document.getElementById("destinationAddressRemoveButton").style.display = "block";
            }
        }
        function addElement(parentId, elementTag, elementId, html) {
            // Adds an element to the document
            var p = document.getElementById(parentId);
            var newElement = document.createElement(elementTag);
            newElement.setAttribute('id', elementId);
            newElement.innerHTML = html;
            p.appendChild(newElement);
        }
        function addDestination() {
            increaseRowId();
            var html = '<input type="text" class="form-control" id="destinationAddress_'+rowId+'" name="destinationAddress_'+rowId+'" /> ' ;
            addElement('destinationAddress', 'p', 'destinationAddress_' + rowId, html);

            resetButton();
        }
        function removeElement() {
            // Removes an element from the document

            var element = document.getElementById('destinationAddress_' + rowId);


            element.parentNode.removeChild(element);

            decreaseRowId();
            resetButton();
        }
    </script>
</head>
<body onload="resetButton();">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link href="https://fonts.googleapis.com/css?family=Oleo+Script:400,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Teko:400,700" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

<section id="contact">
    <div class="section-content">
        <h1 class="section-header"><%--Get in --%><span class="content-header wow fadeIn " data-wow-delay="0.2s" data-wow-duration="2s">Route Finder (connect me at skype, id: alaminphysics)</span></h1>
        <h3>Fill the details below to get delivery routes</h3>
    </div>
    <div class="contact-section">
        <div class="container">
            <form method="post" action="routeMap">
                <div class="col-md-6 form-line">
                    <div class="form-group">
                        <label for="exampleInputUsername">Maximum Delivery Time</label>
                        <input type="number" name="totalTime" class="form-control" id="" placeholder=" Enter Time In Minutes">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail">Starting Address</label>
                        <input type="text" name="startingAddress" class="form-control" id="exampleInputEmail" placeholder=" Enter Starting Address">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail">Time Spend per Destination (in minutes)</label>
                        <input type="number" name="perTime" class="form-control"  placeholder=" Enter Time In Minutes">
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="form-group" id="destinationAddress">
                        <label for="telephone">Destination Addresses</label>
                    </div>
                    <div class="form-group">
                        <p><input type="button" class="form-control btn-primary" value="Add New Destination" onclick="addDestination()"/></p>
                        <p><input type="button" id="destinationAddressRemoveButton" class="form-control btn-primary" value="Remove Destination" onclick="removeElement()"/></p>
                    </div>
                    <div>

                        <button style="margin-bottom: 10px;" type="submit" class="btn btn-success submit"><i class="fa fa-paper-plane" aria-hidden="true"></i>  Find Route</button>
                    </div>

                </div>
            </form>
        </div>
</section>

<!-- header-area end -->
<div class="container-fluid">
    <div class="row">

        <div class="col-xl-12 col-lg-9 col-12">
            <div class="main-content">

                <div id="contact-google-map" data-map-lat="40.925372" data-map-lng="-74.276544" data-icon-path="resources/assets/images/map-icon.png" data-map-title="Awesome Place" data-map-zoom="12"></div>
            </div>
        </div>
    </div>
</div>

<section id="contact">
    <div class="section-content">
        <h3>Routes</h3>
        <h3>Deivery Duration : 120 Minutes</h3>
        <h3>Delivery Boys Needed: 5</h3>
    </div>
    <div class="contact-section">
        <div class="container">

                <div class="col-md-12">
                    <table class="table table-responsive">
                        <tr class="rounded-circle">
                        <th>Delivery Boy 1</th>
                        <th>Delivery Boy 2</th>
                        <th>Delivery Boy 3</th>
                        <th>Delivery Boy 4</th>
                        <th>Delivery Boy 5</th>
                        </tr>
                        <tr>
                            <td>Location 1</td>
                            <td>Location 1</td>
                            <td>Location 1</td>
                            <td>Location 1</td>
                            <td>Location 1</td>
                        </tr>
                        <tr>
                            <td>Location 2</td>
                            <td>Location 2</td>
                            <td>Location 2</td>
                            <td>Location 2</td>
                            <td>Location 2</td>
                        </tr>
                        <tr>
                            <td>Location 3</td>
                            <td>Location 3</td>
                            <td>Location 3</td>
                            <td>Location 3</td>
                            <td>Location 3</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>Location 4</td>
                            <td></td>
                            <td>Location 4</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>Location 5</td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>

                </div>
        </div>
</section>

<<!-- jquery latest version -->
<script src="resources/assets/js/vendor/jquery-2.2.4.min.js"></script>
<!-- bootstrap js -->
<script src="resources/assets/js/bootstrap.min.js"></script>
<!-- waypoints.min.js -->
<script src="resources/assets/js/waypoints.min.js"></script>
<!-- jquery.counterup.min.js -->
<script src="resources/assets/js/jquery.counterup.min.js"></script>
<!-- select2.min.js -->
<script src="resources/assets/js/select2.min.js"></script>
<!-- Google map js -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBevTAR-V2fDy9gQsQn1xNHBPH2D36kck0&callback=initMap"></script>
<!-- Gmap Helper -->
<script src="resources/assets/js/gmap.js"></script>
<!-- Gmap Helper -->
<script src="resources/assets/js/map-script.js"></script>
<!-- main js -->
<script src="resources/assets/js/scripts.js"></script>
</body>
</html>