<%@ page import="com.routefinder.connection.Database" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="static com.routefinder.web.util.Utils.*" %>
<%--
  Created by IntelliJ IDEA.
  User: md_al
  Date: 13-Apr-18
  Time: 4:49 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Employee Tracker</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="resources/assets/images/favicon.png">
    <!-- Place favicon.ico in the root directory -->
    <!-- all css here -->
    <!-- bootstrap v3.3.7 css -->
    <link rel="stylesheet" href="resources/assets/css/bootstrap.min.css">
    <!-- font-awesome.min.css -->
    <link rel="stylesheet" href="resources/assets/css/font-awesome.min.css">
    <!-- select2.min.css -->
    <link rel="stylesheet" href="resources/assets/css/select2.min.css">
    <!-- style css -->
    <link rel="stylesheet" href="resources/assets/css/styles.css">
    <!-- responsive css -->
    <link rel="stylesheet" href="resources/assets/css/responsive.css">
    <!-- modernizr css -->
    <script src="resources/assets/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>

  <%

  Database db = new Database();
  db.connect();
  try {
      session.setAttribute("id",null);
      session.setAttribute("username",null);
      session.setAttribute("userType",null);
      if(!isEmplyOrNull(request.getParameter("username"))) {
      Statement st = db.connection.createStatement();
      String q = "select * from user where username='"+request.getParameter("username")+"' and user_type in ('admin', 'manager')";
      ResultSet rs = st.executeQuery(q);

      if(rs.next()) {
          if(rs.getString("password").equals(request.getParameter("password"))) {
              session.setAttribute("id",rs.getString("id"));
              session.setAttribute("username",rs.getString("username"));
              session.setAttribute("userType",rs.getString("user_type"));
              session.setAttribute("sm", "Successfully Logged In as: "+rs.getString("username"));
              if(rs.getString("user_type").equals("admin")) {
                  response.sendRedirect("dashboard");
              } else {
                  response.sendRedirect("dashboard2");
              }
          } else {
            session.setAttribute("em", "Username and Password doesn't Match!");
          }
      } else {
          session.setAttribute("em", "Username doesn't Exists!");
      }

    }

  %>


  <!--[if lt IE 8]>
  <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->

  <div class="login-area">
      <div class="container">
          <div class="row">
              <div class="col-xl-4 col-md-6 col-12 offset-xl-2">
                  <div class="logo text-center">
                      <a href="dashboard.html">
                          <img src="resources/assets/images/logo.png" alt="">
                      </a>
                  </div>
              </div>
              <div class="col-md-6 line col-12">
                  <div class="login-form text-center">
                      <h2>welcome</h2>
                      <form action="login" method="post">
                          <input type="text" name="username" placeholder="Email">
                          <input type="password" name="password" placeholder="Password">
                          <button type="submit">Log IN</button>
                          <%--<a href="#">Forget Password</a>--%>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>

  <!-- jquery latest version -->
  <script src="resources/assets/js/vendor/jquery-2.2.4.min.js"></script>
  <!-- bootstrap js -->
  <script src="resources/assets/js/bootstrap.min.js"></script>
  <!-- waypoints.min.js -->
  <script src="resources/assets/js/waypoints.min.js"></script>
  <!-- select2.min.js -->
  <script src="resources/assets/js/select2.min.js"></script>
  <!-- jquery.counterup.min.js -->
  <script src="resources/assets/js/jquery.counterup.min.js"></script>
  <!-- select2.min.js -->
  <script src="resources/assets/js/select2.min.js"></script>
  <!-- main js -->
  <script src="resources/assets/js/scripts.js"></script>


  <%} catch (Exception e) {
    System.out.println(e);
  } finally {
       db.close();
  }

  %>
</body>
</html>
