<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="com.routefinder.web.util.Utils" %>
<%@ page import="com.routefinder.connection.Database" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="javax.management.Query" %>
<%@ page import="javax.rmi.CORBA.Util" %><%--
  Created by IntelliJ IDEA.
  User: md_al
  Date: 14-Apr-18
  Time: 4:23 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Employee Tracker</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- all css here -->
    <!-- bootstrap v3.3.7 css -->
    <link rel="stylesheet" href="resources/assets/css/bootstrap.min.css">
    <!-- font-awesome.min.css -->
    <link rel="stylesheet" href="resources/assets/css/font-awesome.min.css">
    <!-- select2.min.css -->
    <link rel="stylesheet" href="resources/assets/css/select2.min.css">
    <!-- style css -->
    <link rel="stylesheet" href="resources/assets/css/styles.css">
    <!-- responsive css -->
    <link rel="stylesheet" href="resources/assets/css/responsive.css">
    <!-- modernizr css -->
    <script src="resources/assets/js/vendor/modernizr-2.8.3.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script>
        $(document).ready(function() {
            $.ajaxSetup({ cache: false }); // This part addresses an IE bug.  without it, IE will only load the first number and will never refresh
            setInterval(function() {
                /*$('#maindiv').load('tracking');*/
                history.go(0);

            }, 20000); // the "3000"
        });




        var rad = function(x) {
            return x * Math.PI / 180;
        };

        var getDistance = function(lat1, lng1, lat2, lng2) {
            var R = 6378137; // Earth’s mean radius in meter
            var dLat = rad(lat2 - lat1);
            var dLong = rad(lng2 - lng1);
            var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(rad(lat1)) * Math.cos(rad(lat2)) *
                Math.sin(dLong / 2) * Math.sin(dLong / 2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            var d = R * c;
            return d; // returns the distance in meter
        };
        function populateEmployee() {
            window.location.href='live-tracking2?employee='+document.getElementById("employee").value;
        }
    </script>
</head>

<body>
<%
    if(Utils.isEmplyOrNull(session.getAttribute("username"))) {
        response.sendRedirect("login");
    }

    Database db = new Database();
    db.connect();

    try{

        boolean isAdmin = !Utils.isEmplyOrNull(session.getAttribute("userType")) && session.getAttribute("userType").equals("admin");



%>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!-- header-area start -->
<header class="header-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-8 col-12">
                <div class="header-left">
                    <%if(isAdmin){%>
                    <h5><a href="dashboard"><i class="fa fa-home"></i></a> Employee Work Tracking</h5>
                    <%}else{%>
                    <h5><a href="dashboard2"><i class="fa fa-home"></i></a> Employee Work Tracking</h5>
                    <%}%>
                </div>
            </div>
            <div class="col-lg-6 col-md-5 col-sm-3 col-8">
                <div class="header-right text-right">
                    <a href="login"><i class="fa fa-sign-out"></i>Logout</a>
                </div>
            </div>
            <div class="col-md-1 col-sm-1 d-block d-lg-none col-4">
                <ul class="menu">
                    <li class="first"></li>
                    <li class="second"></li>
                    <li class="third"></li>
                </ul>
            </div>
        </div>
    </div>
</header>
<!-- header-area end -->
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-2 col-lg-3 col-12">
            <!-- sidebar-style start -->
            <div class="sidebar-style">
                <div class="sliderbar-area">
                    <h2 class="slidebar-title">Hi <%=session.getAttribute("username")%></h2>
                    <div class="profile-img">
                        <img src="resources/assets/images/<%=session.getAttribute("id")%>.jpg" alt="Profile Picture">
                        <div class="profile-content">
                            <form method="post" action="profile" enctype="multipart/form-data">
                                <input type="hidden" name="uploadImage" value="<%=session.getAttribute("id")%>"/>
                                <input type="file" name="file" required/>
                                <button type="submit"><i class="fa fa-camera"></i>Upload Image</button>
                            </form>
                        </div>
                    </div>
                    <div class="mainmenu">
                        <ul>
                            <%if(isAdmin){%>
                            <li><a href="dashboard">Dashboard</a></li>
                            <li><a href="account-info">Profiles</a></li>
                            <li><a href="account">Accounts</a></li>
                            <li class="active"><a href="live-tracking">Live Tracking</a></li>
                            <%}else {%>
                            <li ><a href="dashboard2">Dashboard</a></li>
                            <li><a href="account-info">Profiles</a></li>
                            <li><a href="account">Accounts</a></li>
                            <li class="active"><a href="live-tracking2">Live Tracking</a></li>
                            <%}%>
                            <li><a href="reports">History</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- sidebar-style end -->
        </div>
        <div class="col-xl-10 col-lg-9 col-12">
            <div class="main-content">
                <div class="row">
                    <div class="col-xl-2 col-lg-3 col-12">
                        <div class="select-form-style">
                            <%




                                Statement st5 = db.connection.createStatement();
                                ResultSet rs5 = st5.executeQuery("select * from user where  user_type='employee' and assigned_manager="+session.getAttribute("id"));

                            %>
                            <%--<h3>Select Employee</h3>--%>
                            <select class="js-example-basic-single" id="employee" name="employee" onchange="populateEmployee();">
                                <option <%if(request.getParameter("employee")==null||request.getParameter("employee").equals("")){%>selected<%}%> value="">Select Employee</option>
                                <%while (rs5.next()) {%>

                                <option <%if(request.getParameter("employee")!=null&&request.getParameter("employee").equals(rs5.getString("id"))){%>selected<%}%> value="<%=rs5.getString("id")%>"><%=rs5.getString("name")%></option>
                                <%}%>
                            </select>
                        </div>
                    </div>

                    <%
                        String employeeId="-1";

                        if(!Utils.isEmplyOrNull(request.getParameter("employee"))) {
                            employeeId=request.getParameter("employee");
                        }

                        String q = "select * from event join user on(user.id=event.user_id) where" +
                                " user.assigned_manager="+session.getAttribute("id")+" and user.id="+employeeId;

                        q+=" and substr(event_time,1,10) like '"+Utils.getTimeStrNow().substring(0,10)+"' ";
                        q+=" ORDER by event_time desc";


                    %>

                    <%

                        Statement st = db.connection.createStatement();
                        ResultSet rs = st.executeQuery(q);
                        String centerLat = "23.363";
                        String centerLong = "90.044";
                        double distCovered = 0.0;
                        if(rs.next()) {
                            centerLat = rs.getString("latitude");
                            centerLong = rs.getString("longitude");
                            distCovered = 0.0;
                        }
                    %>
                    <%--<div id="map"></div>--%>

                    <script>

                        function initMap() {

                            var infowindow = new google.maps.InfoWindow();
                            var i=0;
                            var center = new google.maps.LatLng(<%=centerLat%>, <%=centerLong%>);
                            var map = new google.maps.Map(document.getElementById('contact-google-map2'), {
                                zoom: 16,
                                center: center
                            });
                            var distCovered = 0.0;
                            <%
                            rs.beforeFirst();
                            String eventType="";
                            while (rs.next()) {%>
                            (function(){

                                var icon = {
                                    url: "resources/assets/images/<%=rs.getString("user_id")%>.jpg", // url
                                    scaledSize: new google.maps.Size(40, 40), // scaled size
                                    origin: new google.maps.Point(0, 0), // origin
                                    anchor: new google.maps.Point(0, 0) // anchor
                                };

                                var markerIconGlastonbury = {
                                    url: "resources/assets/images/<%=rs.getString("user_id")%>.jpg",
                                    //The size image file.
                                    size: new google.maps.Size(225, 120),
                                    //The point on the image to measure the anchor from. 0, 0 is the top left.
                                    origin: new google.maps.Point(0, 0),
                                    //The x y coordinates of the anchor point on the marker. e.g. If your map marker was a drawing pin then the anchor would be the tip of the pin.
                                    anchor: new google.maps.Point(<%=rs.getString("latitude")%>, <%=rs.getString("longitude")%>)
                                };

                                //Setting the shape to be used with the Glastonbury map marker.
                                var markerShapeGlastonbury = {
                                    coord: [12,4,216,22,212,74,157,70,184,111,125,67,6,56],
                                    type: 'poly'
                                };

                                var marker = new google.maps.Marker({
                                    position: new google.maps.LatLng(<%=rs.getString("latitude")%>, <%=rs.getString("longitude")%>),
                                    map: map,
                                    icon: icon,
                                    shape: markerShapeGlastonbury,
                                    //sets the z-index of the map marker.
                                    zIndex:102
                                });




                                var flightPath = new google.maps.Polyline({
                                    path: [{lat:<%=centerLat%>, lng:<%=centerLong%>},
                                        {lat:<%=rs.getString("latitude")%>, lng:<%=rs.getString("longitude")%>}],
                                    geodesic: true,
                                    strokeColor: '#FF0000',
                                    strokeOpacity: 1.0,
                                    strokeWeight: 2
                                });
                                <%if(!rs.getString("event_type").equals("pause")&&!rs.getString("event_type").equals("end")){%>

                                flightPath.setMap(map);
                                distCovered+=getDistance(<%=centerLat%>, <%=centerLong%>,<%=rs.getString("latitude")%>, <%=rs.getString("longitude")%>);

                                <%
                                }else {%>
                                distCovered=0.0;
                                <%}
                                eventType=rs.getString("event_type");%>
                                <%

                                centerLat = rs.getString("latitude");
                                centerLong = rs.getString("longitude");
                                %>


                                marker.addListener('click', function() {

                                    map.setCenter(marker.getPosition());
                                });

                                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                                    return function () {

                                        infowindow.setContent("<strong>time : "+"<%=rs.getString("event_time")%>"+"</strong>"+"<br>"+"<b>event : "+"<%=rs.getString("event_type")%>"+"</b><br><b>Distance Covered: "+distCovered+" meters</b>");

                                        infowindow.open(map, marker);

                                    }
                                })(marker, i));
                                i++;
                            })();
                            <%}%>
                        }
                    </script>


                    <div class="col-xl-10 col-lg-9">
                        <div id="contact-google-map2"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- jquery latest version -->
<script src="resources/assets/js/vendor/jquery-2.2.4.min.js"></script>
<!-- bootstrap js -->
<script src="resources/assets/js/bootstrap.min.js"></script>
<!-- waypoints.min.js -->
<script src="resources/assets/js/waypoints.min.js"></script>
<!-- jquery.counterup.min.js -->
<script src="resources/assets/js/jquery.counterup.min.js"></script>
<!-- Google map js -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBevTAR-V2fDy9gQsQn1xNHBPH2D36kck0&callback=initMap"></script>
<!-- Gmap Helper -->
<script src="resources/assets/js/gmap.js"></script>
<!-- Gmap Helper -->
<script src="resources/assets/js/map-script.js"></script>
<!-- select2.min.js -->
<script src="resources/assets/js/select2.min.js"></script>
<!-- main js -->
<script src="resources/assets/js/scripts.js"></script>

<%} catch (Exception e) {
    System.out.println(e);
} finally {
    db.close();
}

%>
</body>

</html>