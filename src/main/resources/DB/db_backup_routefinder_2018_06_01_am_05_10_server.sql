-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: routefinder
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `id` int(11) DEFAULT NULL,
  `address` varchar(1010) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (0,'Mishkan Shilo Street 2, Jerusalem'),(1,'A.D. Gordon Street 12, Jerusalem'),(2,'A.D. Gordon Street 18, Jerusalem'),(3,'Borochov Street 18, Jerusalem'),(4,'Borochov Street 26, Jerusalem'),(5,'Borochov Street 32, Jerusalem'),(6,'Borochov Street 38, Jerusalem'),(7,'Borochov Street 50, Jerusalem'),(8,'Leon Kubovi Street 36, Jerusalem'),(9,'Leon Kubovi Street 12, Jerusalem'),(10,'Abel Pann Street 1, Jerusalem'),(11,'Gelber Street 12, Jerusalem'),(12,'Gelber Street 15, Jerusalem'),(13,'Gelber Street 20, Jerusalem'),(14,'HaAyal Street 27, Jerusalem'),(15,'HaAyal Street 4, Jerusalem'),(16,'HaAyal Street 69, Jerusalem'),(17,'HaAyal Street 11, Jerusalem'),(18,'HaKfir Street 195, Jerusalem'),(19,'HaKfir Street 3, Jerusalem'),(20,'HaDishon Street 4, Jerusalem'),(21,'HaDishon Street 30, Jerusalem'),(22,'HaKfir Street 14, Jerusalem'),(23,'Horkanya Street 20, Jerusalem'),(24,'Horkanya Street 3, Jerusalem'),(25,'Horkanya Street 10, Jerusalem'),(26,'Hose San Martin Street 7, Jerusalem'),(27,'Yitshak Sadeh Street 5, Jerusalem'),(28,'Tiltan Street 4, Jerusalem'),(29,'Tiltan Street 9, Jerusalem'),(30,'Tiltan Street 11, Jerusalem'),(31,'Tiltan Street 21, Jerusalem'),(32,'Kassuto Street 2, Jerusalem'),(33,'Kassuto Street 7, Jerusalem'),(34,'Kassuto Street 14, Jerusalem'),(35,'Kassuto Street 17, Jerusalem'),(36,'Ha-Pisga Street 1, Jerusalem'),(37,'Ha-Pisga Street 17, Jerusalem'),(38,'Ha-Pisga Street 34, Jerusalem'),(39,'Ha-Pisga Street 44, Jerusalem'),(40,'Ha-Pisga Street 53, Jerusalem'),(41,'Ha-Pisga Street 67, Jerusalem'),(42,'Guatemala Sreet 2, Jerusalem'),(43,'Guatemala Sreet 12, Jerusalem'),(44,'Guatemala Sreet 19, Jerusalem'),(45,'Guatemala Sreet 23, Jerusalem'),(46,'Avraham Stern Street 78, Jerusalem'),(47,'Avraham Stern Street 53, Jerusalem'),(48,'Avraham Stern Street 32, Jerusalem'),(49,'Avraham Stern Street 14, Jerusalem'),(50,'HaNerd Street 7, Jerusalem'),(51,'HaNerd Street 23, Jerusalem'),(52,'HaNerd Street 30, Jerusalem'),(53,'HaNerd Street 39, Jerusalem'),(54,'Costa Rica Street 81, Jerusalem'),(55,'Costa Rica Street 1, Jerusalem'),(56,'Costa Rica Street 23, Jerusalem'),(57,'Costa Rica Street 57, Jerusalem'),(58,'Costa Rica Street 71, Jerusalem'),(59,'HaSavyon Street 10, Jerusalem'),(60,'HaSavyon Street 1, Jerusalem'),(61,'HaSavyon Street 16, Jerusalem'),(62,'HaSavyon Street 7, Jerusalem'),(63,'Iceland Street 1, Jerusalem'),(64,'Iceland Street 17, Jerusalem'),(65,'Iceland Street 32, Jerusalem'),(66,'Iceland Street 38, Jerusalem'),(67,'Iceland Street 42, Jerusalem'),(68,'Immanuel Olsvenger Street 1, Jerusalem'),(69,'Immanuel Olsvenger Street 17, Jerusalem'),(70,'Immanuel Olsvenger Street 32, Jerusalem'),(71,'Immanuel Olsvenger Street 45, Jerusalem'),(72,'Immanuel Olsvenger Street 55, Jerusalem'),(73,'Immanuel Olsvenger Street 69, Jerusalem'),(74,'Shakhal Street 10, Jerusalem'),(75,'Shakhal Street 57, Jerusalem'),(76,'Ha-Rav Khayim Heler 4, Jerusalem'),(77,'Ha-Rav Khayim Heler 23, Jerusalem'),(78,'Tora VaAvoda Street 1, Jerusalem'),(79,'Tora VaAvoda Street 4, Jerusalem');
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `current`
--

DROP TABLE IF EXISTS `current`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `current` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `per_time` varchar(110) NOT NULL,
  `starting_address` varchar(1010) NOT NULL,
  `total_time` varchar(110) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `current_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `current`
--

LOCK TABLES `current` WRITE;
/*!40000 ALTER TABLE `current` DISABLE KEYS */;
INSERT INTO `current` VALUES (1,'30','Mishkan Shilo Street 2, Jerusalem','120');
/*!40000 ALTER TABLE `current` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `boy_id` int(11) DEFAULT NULL,
  `loc_str` varchar(10000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (0,'0_-1_0.0_0:36_0_735.4427005322818_280:37_36_188.20358144790418_80:79_37_207.6699425265972_62:38_79_102.36611635371077_33:39_38_82.64632391583135_35:40_39_137.47821792388683_50:41_40_142.79107928235896_40:32_41_216.55843961935238_266:33_32_45.54797757803306_133:34_33_55.73951734476379_132:35_34_95.24002388727565_124:30_35_220.3111698098269_195:29_30_33.383589381070855_11:31_29_0.0_0:28_31_104.69336721306409_41:10_28_334.43135202671823_288:11_10_168.73767791950925_44:12_11_57.86107535407152_19:13_12_22.105834057697706_3:9_13_87.54901736219759_119:8_9_244.35004631408418_52:42_8_444.81935911847995_305:43_42_310.29842064257775_129:44_43_89.17117423234083_21:45_44_99.24264946250386_25:49_45_158.98184802448318_275:46_49_316.2993897821408_231:47_46_145.4179474991104_24:48_47_189.7875856149701_50:20_48_362.33948956213493_309:16_20_215.50647294756_45:18_16_369.6730960279089_159:14_18_214.7083904412938_128:21_14_118.19752446680826_179:17_21_305.1028714970092_214:15_17_41.90615653750095_6:22_15_57.54330710876293_26:19_22_38.72427201427812_6:26_19_663.587141394322_362:27_26_388.1793892168654_180:24_27_567.566154410658_204:25_24_83.19326390017123_22:23_25_152.92012709207273_50:74_23_488.5811466601911_324:77_74_231.35145120304696_132:75_77_397.3420890209146_143:76_75_151.36510381842723_64'),(1,'0_-1_0.0_0:78_0_1205.8484280013968_378:1_78_287.431071740843_200:2_1_95.4826886720359_172:6_2_72.99735414258822_163:5_6_57.84983110979023_178:4_5_64.96080591405135_175:3_4_103.38830382383533_168:71_3_361.0656403069314_348:72_71_0.0_0:70_72_63.6330444339723_165:73_70_71.76694252221237_12:69_73_98.58623556137645_35:68_69_167.09465765017958_50:7_68_575.226351788247_363:53_7_964.4022615171644_482:51_53_106.06286240663401_243:52_51_19.091970842261645_209:56_52_181.30720247506062_199:57_56_231.7715299767628_34:54_57_372.22325984806366_64:58_54_0.0_0:55_58_322.8639244331532_127:50_55_178.5227653162959_85:63_50_207.6399843256559_231:60_63_283.29897455892217_103:59_60_117.75298503461455_25:62_59_10.20680813915887_1:61_62_94.1955636247235_19:64_61_582.3720781799117_263:66_64_47.799681121972704_58:65_66_43.16220963370362_11:67_65_19.532694145960786_3');
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(110) NOT NULL,
  `password` varchar(110) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  `designation` varchar(110) DEFAULT NULL,
  `details` varchar(110) DEFAULT NULL,
  `user_type` varchar(110) NOT NULL DEFAULT 'employee',
  `assigned_manager` int(11) DEFAULT '0',
  `branch` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_uindex` (`id`),
  UNIQUE KEY `user_username_uindex` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-01 13:11:26
